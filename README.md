# Feather Brief

Feather Brief est un aggrégateur de contenu RSS.

## Développement

```bash
$ npm install
$ VUE_APP_USE_REAL_AUTH=true \
  VUE_APP_USE_REAL_DOMAIN=true \
  #nécesaire pour l'authentification
  PORT=3000 \
  npm run serve
```

Pendant les tests, on utilise un système d'authentification
mocké, pour ne pas dépendre de l'authentification via Auth0,
et on utilise un faux serveur, pour ne pas dépendre de données
issues du serveur par défaut.

Un serveur de substitution est fourni, la gestion des réponses
est disponible dans le fichier [disable-network.ts](src/disable-network.ts).
L'authentification sera désactivé, dans un environnement de test
ou de développement par défaut. On utilise le flag
`VUE_APP_USE_REAL_AUTH` pour désactiver ce comportement,
voir le fichier [auth.ts](src/plugins/auth/auth.ts).

Pour désactiver ce comportement, on peut créer un [fichier
d'environnement][u:envvar] local `.env.local`, dans lequel on trouvera
le contenu suivant:

    PORT=3000
    VUE_APP_USE_REAL_DOMAIN=true
    VUE_APP_USE_REAL_AUTH=true

Autrement, on peut utiliser la ligne de commande fournie
en exemple plus haut.

[u:envvar]: https://cli.vuejs.org/guide/mode-and-env.html#environment-variables

## Technologies principales

L'application utilise [Vue.js](https://vuejs.org) comme
librairie de rendu front-end principale. La navigation
entre les pages exploite les mécanismes de
[vuejs-router](https://router.vuejs.org), et
les composants sont issues de la bibliothèque
[vuetify](https://vuetifyjs.com). Enfin, une portion de
l'état de l'application est géré par un store
[vuex](https://vuex.vuejs.org).

L'authentification utilise [Auth0](https://auth0.com/fr/).

## FAQ

**1. Combien coute Feather Brief**

Feather Brief est complètement libre et gratuit à l'utilisation.
Ses créateurs utilisent leur temps libre et leurs propres ressources
pour gérer l'application. Pour le moment, la meilleure façon de nous
aider est d'utiliser l'application, et de nous faire remonter
avec bienveillance vos retours.

Le fait que l'application soit gratuite implique aussi qu'il n'y a aucun
engagement de disponibilité, ou qu'un bug soit résolu avant un autre.

**2. Puis-je héberger l'application sur mon propre serveur ?**

L'idée de Feather Brief est de fournir un système qui soit facile
à installer dans sa propre infrastructure.
Pour le moment, ce but n'est pas complètement atteint, mais nous
y travaillons. Lorsque ce sera le cas, nous fournirons un guide
d'installation plus détaillé.

En ce qui concerne l'application web, il s'agit d'une simple
application statique, que l'on peut héberger, la plupart du temps
gratuitement en ligne (c'est le cas encore aujourd'hui).
Pour le moment, nous conservons ce choix, parce qu'il nous facilite
le déploiement automatisé de cette portion de l'application, mais
il n'est pas impossible que cela change dans le futur.

**3. Comment faire remonter une anomalie ?**

Le code source de l'application est complètement ouvert, ainsi que
notre outil de gestion des anomalies et évolutions que l'on peut
retrouver ici: https://gitlab.com/feather-brief/feather-brief-vue-app/-/issues.
Le plus simple est d'ouvrir un ticket en taggant @jquagliatini, et en ajoutant
le tag `BUG`.

**4. J'ai besoin de conseil en développement**

Ça tombe bien ! Les 2 créateurs de l'application proposent leurs services
à tous, ils seront heureux que vous leur fassiez part de
vos problématiques. Retrouvez leur Linkedin respectifs:

- [in/jordan-quagliatini](https://www.linkedin.com/in/jordan-quagliatini)
- [in/constant-deschietere](https://www.linkedin.com/in/constant-deschietere)
