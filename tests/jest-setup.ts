import { config } from "@vue/test-utils";

config.mocks.$t = (key: string) => key;
config.mocks.$tc = (key: string, n: number) => `${n} ${key}`;
config.mocks.$n = (n: number) => n;
