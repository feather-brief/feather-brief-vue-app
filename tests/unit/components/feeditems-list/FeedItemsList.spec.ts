import Vuetify from "vuetify";
import Vue from "vue";
import { shallowMount } from "@vue/test-utils";
import { FeedWithItemsGenerator } from "@/../tests/unit/utils/FeedWithItemsGenerator";
import { FeedItem } from "@/domain/FeedItem";
import FeedItemsList from "@/components/feeditems-list/FeedItemsList.vue";

Vue.use(Vuetify);

let feeditems: FeedItem[];

beforeAll(() => {
  feeditems = new FeedWithItemsGenerator().withItems(2).generate().items;
});

test("it should render without replace", () => {
  const wrapper = shallowMount(FeedItemsList, {
    propsData: {
      feeditems,
    },
  });

  expect(wrapper).toMatchSnapshot();
});

test("it should render with replace", () => {
  const wrapper = shallowMount(FeedItemsList, {
    propsData: {
      feeditems: [feeditems[0]],
    },
  });

  expect(wrapper).toMatchSnapshot();
});
