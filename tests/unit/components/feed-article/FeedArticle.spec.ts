import Vue from "vue";
import Vuetify from "vuetify";
import { mount } from "@vue/test-utils";
import FeedArticle from "@/components/feed-article/FeedArticle.vue";
import { FeatherBriefArticleParsed } from "@/plugins/feather-brief/IFeatherBrief";
import { FeedItem } from "@/domain/FeedItem";
import { FeedItemGenerator } from "@/../tests/unit/utils/FeedWithItemsGenerator";
import {
  Getters,
  useReaderSettingsStore,
} from "@/store/modules/reader-settings";

let article: FeatherBriefArticleParsed;
let feedItem: FeedItem;

Vue.use(Vuetify);

beforeEach(() => {
  const content =
    "<h2>Article content</h2><section><p>Here is some content</p></section>";
  feedItem = new FeedItemGenerator().generate();
  feedItem.articleUrl = "https://awesome-website.com/article/something-awesome";
  feedItem.title = "Article content";
  article = {
    content,
    byline: "byline",
    excerpt: "excertp",
    length: content.length,
    siteName: "Awesome website",
    title: feedItem.title,
  };
});

const $auth = { isAuthenticated: true };
const $router = { resolve: () => "https://example.com" };
const $route = {
  params: { feedName: "Feed Name" },
};
const $store = {
  state: {
    feedsWithItems: [],
  },
  getters: {
    [Getters.STYLE]: (useReaderSettingsStore()?.state as any)?.style ?? {},
  },
};

const getWrapper = (props: any = {}) => {
  return mount(FeedArticle, {
    vuetify: new Vuetify(),
    mocks: {
      $auth,
      $router,
      $route,
      $store,
    },
    propsData: props,
  });
};

test("with parsable article", () => {
  feedItem.parsable = true;

  expect(
    getWrapper({
      article,
      item: feedItem,
    })
  ).toMatchSnapshot();
});

test("with unparsable article", () => {
  feedItem.parsable = false;

  expect(
    getWrapper({
      article,
      item: feedItem,
    })
  ).toMatchSnapshot();
});
