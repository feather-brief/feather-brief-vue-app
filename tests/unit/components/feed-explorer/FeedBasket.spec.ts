import { shallowMount, createLocalVue, Wrapper, mount } from "@vue/test-utils";
import Vue from "vue";
import Vuex from "vuex";
import Vuetify from "vuetify";
import FeedBasket from "@/components/feed-explorer/FeedBasket.vue";
import { FeedBasketGetters } from "@/store/modules/feed-basket/Getters";
import { FEED_BASKET_MODULE } from "@/store/modules/feed-basket/useFeedBasketStore";

Vue.use(Vuetify);

const localVue = createLocalVue();
localVue.use(Vuex);

describe("FeedBasket", () => {
  let getters: Record<FeedBasketGetters, any>;
  let state: any;
  let store: any;
  let vuetify: Vuetify;
  let wrapper: Wrapper<Vue>;

  beforeEach(() => {
    getters = {
      [FeedBasketGetters.isEmpty]: jest.fn(),
      [FeedBasketGetters.exists]: jest.fn(),
    };

    state = {
      basket: [
        { feedName: "FeedName", feedUrl: "feedUrl" },
        { feedName: "FeedName2", feedUrl: "feedUrl2" },
      ],
    };

    store = new Vuex.Store({
      modules: {
        [FEED_BASKET_MODULE]: {
          state,
          getters,
          mutations: {
            pop(state) {
              state.basket.pop();
            },
          },
        },
      },
    });

    vuetify = new Vuetify();

    wrapper = shallowMount(FeedBasket, {
      store,
      localVue,
      vuetify,
    });
  });

  it("should match shallow snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should match complete mount snapshot", () => {
    wrapper = mount(FeedBasket, { vuetify, store, localVue });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should display the number of items in basket", () => {
    expect(wrapper.find(".basket-items-number").text()).toBe("2");
  });

  it("should update the button label, according to the number of items in basket", async () => {
    wrapper = mount(FeedBasket, { vuetify, localVue, store });
    expect(wrapper.find(".basket-action").text()).toBe(
      `2 components.feed_explorer.basket_snackbar`
    );
    store.commit("pop");
    await wrapper.vm.$nextTick();
    expect(wrapper.find(".basket-action").text()).toBe(
      `1 components.feed_explorer.basket_snackbar`
    );
  });

  it("should emit 'submit' on click", async () => {
    wrapper = mount(FeedBasket, { vuetify, localVue, store });
    wrapper.find(".basket-action").trigger("click");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("submit")).toBeTruthy();
  });

  it("should be invisible if getter `isEmpty` returns true", async () => {
    getters[FeedBasketGetters.isEmpty].mockReturnValue(true);
    wrapper = shallowMount(FeedBasket, {
      vuetify,
      localVue,
      store: new Vuex.Store({
        modules: {
          [FEED_BASKET_MODULE]: {
            state,
            getters,
          },
        },
      }),
    });

    await wrapper.vm.$nextTick();
    expect(getters[FeedBasketGetters.isEmpty]).toHaveBeenCalled();
    expect(wrapper.find(".basket").attributes("value")).toBeUndefined();
  });
});
