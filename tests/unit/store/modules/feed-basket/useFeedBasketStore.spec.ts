import { useFeedBasketStore } from "@/store/modules/feed-basket/useFeedBasketStore";
import { FeedBasketGetters as G } from "@/store/modules/feed-basket/Getters";
import { FeedBasketMutations as M } from "@/store/modules/feed-basket/Mutations";
import { FeedBasketActions as A } from "@/store/modules/feed-basket/Actions";
import { Feed } from "@/domain/Feed";

describe("feedBasketStore", () => {
  const { getters, mutations, actions, state } = useFeedBasketStore();

  describe("state initialization", () => {
    it("should initialize an empty basket", () => {
      expect(state).toEqual({ basket: [] });
    });
  });

  describe("getters", () => {
    describe(G.exists, () => {
      it("should return true for an existing feedUrl", () => {
        expect(
          // @ts-ignore
          getters[G.exists]({ basket: [{ feedUrl: "feedUrl" }] })({
            feedUrl: "feedUrl",
          })
        ).toBe(true);
      });

      it("should return false for a non existing feedUrl", () => {
        expect(
          // @ts-ignore
          getters[G.exists]({
            basket: [],
          })({ feedUrl: "feedUrl" })
        ).toBe(false);
      });
    });

    describe(G.isEmpty, () => {
      it("should return true for an empty basket", () => {
        expect(
          // @ts-ignore
          getters[G.isEmpty]({ basket: [] })
        ).toBe(true);
      });

      it("should return false for a non empty basket", () => {
        expect(
          // @ts-ignore
          getters[G.isEmpty]({ basket: [{ feedUrl: "feedUrl" }] })
        ).toBe(false);
      });
    });
  });

  describe("mutations", () => {
    describe(M.add, () => {
      it("should add a new feed to the basket", () => {
        const state = { basket: [] };
        // @ts-ignore
        mutations[M.add](state, { feedUrl: "feedUrl" });
        expect(state.basket).toHaveLength(1);
      });
    });

    describe(M.remove, () => {
      it("should remove an existing feed to the basket", () => {
        const state = {
          basket: [{ feedUrl: "feedUrl" }, { feedUrl: "feedUrl#2" }],
        };
        // @ts-ignore
        mutations[M.remove](state, { feedUrl: "feedUrl" });
        expect(state).toStrictEqual({
          basket: [{ feedUrl: "feedUrl#2" }],
        });
      });

      it("should let state as it is if the feed does not exist", () => {
        const state = {
          basket: [{ feedUrl: "feedUrl" }],
        };
        // @ts-ignore
        mutations[M.remove](state, { feedUrl: "feedUrl#1" });
        expect(state).toStrictEqual({
          basket: [{ feedUrl: "feedUrl" }],
        });
      });
    });

    describe(M.empty, () => {
      it("should empty the current basket", () => {
        const state = { basket: [{ feedUrl: "feedUrl" }] };
        // @ts-ignore
        mutations[M.empty](state);

        expect(state.basket).toHaveLength(0);
      });

      it("should empty even an empty basket", () => {
        const initialBasket: Feed[] = [];
        const state = { basket: initialBasket };

        // @ts-ignore
        mutations[M.empty](state);

        expect(state.basket).not.toBe(initialBasket);
      });
    });
  });

  describe("actions", () => {
    const ctx = {
      commit: jest.fn(),
    };

    describe(A.addFeed, () => {
      it("should commit add feed mutation", async () => {
        const feed = { feedUrl: "feedUrl", feedName: "feedName" };
        // @ts-ignore
        await actions[A.addFeed](ctx, feed);
        expect(ctx.commit).toHaveBeenCalledWith(M.add, feed);
      });
    });

    describe(A.removeFeed, () => {
      it("should commit remove feed mutation", async () => {
        const feed = { feedUrl: "feedUrl", feedName: "feedName" };
        // @ts-ignore
        await actions[A.removeFeed](ctx, feed);
        expect(ctx.commit).toHaveBeenCalledWith(M.remove, feed);
      });
    });
  });
});
