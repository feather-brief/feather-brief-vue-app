import {
  actions,
  mutations,
  FeatherBriefState,
  MarkUnmarkFeedItemPayload,
  getters,
} from "@/store/useStore";
import { Actions as A } from "@/store/Actions";
import { Mutations as M } from "@/store/Mutations";

import { CachedFeatherBrief } from "@/plugins/feather-brief/CachedFeatherBrief";
import {
  FeedItemGenerator,
  FeedWithItemsArrayGenerator,
  FeedWithItemsGenerator,
} from "../utils/FeedWithItemsGenerator";
import { FeedWithItems } from "@/domain/FeedWithItems";
import { ArticleStatus } from "@/domain/ArticleStatus";
import { Getters } from "@/store/Getters";
import { Feed } from "@/domain/Feed";
import { DEFAULT_CATEGORY, FeedItem } from "@/domain/FeedItem";
import { omit } from "lodash";

describe("actions", () => {
  describe(A.refreshFeedItems, () => {
    const feedWithItems = FeedWithItemsArrayGenerator.n(2).generate();
    const $fb = ({
      invalidate: jest.fn(),
      getLastFeedItems: jest.fn(() => Promise.resolve(feedWithItems)),
    } as Partial<CachedFeatherBrief>) as CachedFeatherBrief;
    // @ts-ignore
    const refreshFeedItems: (ctx: { commit: Function }) => void = actions($fb)[
      A.refreshFeedItems
    ];

    it("should call invalidate() and getLastFeedItems()", async () => {
      const ctx = { commit: jest.fn(), state: { synced: true } };

      await refreshFeedItems(ctx);

      expect($fb.invalidate).toHaveBeenCalled();
      expect($fb.getLastFeedItems).toHaveBeenCalled();

      expect(ctx.commit).toHaveBeenCalledWith(
        M.updateFeedWithItems,
        feedWithItems
      );
    });
  });

  describe(A.deleteFeedItems, () => {
    // @ts-ignore
    const $fb: CachedFeatherBrief = { mark: jest.fn() };
    // @ts-ignore
    const deleteFeedItems: (
      ctx: { commit: Function; state: { feedsWithItems: FeedWithItems[] } },
      urls: string | string[]
    ) => void = actions($fb)[A.deleteFeedItems];

    it("should take a single URL as item to delete", async () => {
      const feedsWithItems = FeedWithItemsArrayGenerator.n(1)
        .first()
        .withItems(2)
        .back()
        .generate();
      const urlToDelete = feedsWithItems[0].items[0].articleUrl;

      const ctx = {
        commit: jest.fn(),
        state: {
          feedsWithItems,
        },
      };

      await deleteFeedItems(ctx, urlToDelete);
      expect(ctx.commit).toHaveBeenCalledWith(M.updateDeleted, [urlToDelete]);
    });

    it("should take an array of URLs as items to delete", async () => {
      const feedsWithItems = FeedWithItemsArrayGenerator.n(2)
        .first()
        .withItems(2)
        .back()
        .nth(2)
        .withItems(2)
        .back()
        .generate();

      const urlsToDelete = feedsWithItems[0].items.map((f) => f.articleUrl);

      const ctx = {
        commit: jest.fn(),
        state: {
          feedsWithItems,
        },
      };

      await deleteFeedItems(ctx, urlsToDelete);
      expect(ctx.commit).toHaveBeenCalledWith(M.updateDeleted, urlsToDelete);
    });

    it("should call mark as SHOWN", async () => {
      const feedsWithItems = FeedWithItemsArrayGenerator.n(1)
        .first()
        .withItems(2)
        .back()
        .generate();
      const urlToDelete = feedsWithItems[0].items[0].articleUrl;

      const ctx = {
        commit: jest.fn(),
        state: {
          feedsWithItems,
        },
      };

      await deleteFeedItems(ctx, urlToDelete);
      expect($fb.mark).toHaveBeenCalledWith([urlToDelete], ArticleStatus.SHOWN);
    });
  });

  describe.each([A.markFeedItems, A.unmarkFeedItems])("%s()", (markUnmark) => {
    const $fb = ({ mark: jest.fn(), unmark: jest.fn() } as Partial<
      CachedFeatherBrief
    >) as CachedFeatherBrief;
    const ctx = { commit: jest.fn() };

    // @ts-ignore
    const markUnmarkFeedItems: (
      ctx: { commit: Function },
      args: MarkUnmarkFeedItemPayload
    ) => Promise<void> = actions($fb)[markUnmark];

    it(`should call $fb.${
      markUnmark === A.unmarkFeedItems ? "un" : ""
    }mark()`, async () => {
      await markUnmarkFeedItems(ctx, {
        urls: ["url"],
        status: ArticleStatus.SHOWN,
      });
      expect(
        markUnmark === A.markFeedItems ? $fb.mark : $fb.unmark
      ).toHaveBeenCalledWith(["url"], ArticleStatus.SHOWN);
    });

    it("should commit udpateStatus", async () => {
      await markUnmarkFeedItems(ctx, {
        urls: ["url"],
        status: ArticleStatus.SHOWN,
      });

      expect(ctx.commit).toHaveBeenCalledWith(M.updateStatus, {
        urls: ["url"],
        status: ArticleStatus.SHOWN,
        value: markUnmark === A.markFeedItems ? true : false,
      });
    });
  });

  describe(A.offloadArticle, () => {
    const $fb = ({
      offloadArticle: jest.fn(),
    } as unknown) as CachedFeatherBrief;
    const offloadArticle = actions($fb)[A.offloadArticle];

    it("should call offload from $fb and commit the status update", async () => {
      const ctx = {
        commit: jest.fn(),
      };

      const feedItem = new FeedItemGenerator().generate();

      // @ts-ignore
      await offloadArticle(ctx, feedItem);

      expect($fb.offloadArticle).toHaveBeenCalledWith(feedItem);
      expect(ctx.commit).toHaveBeenCalledWith(M.updateStatus, {
        urls: [feedItem.articleUrl],
        status: ArticleStatus.SAVED,
        value: false,
      });
    });
  });

  describe(A.loading, () => {
    it("should mark as loading", async () => {
      const state = { loading: false };
      // @ts-ignore
      await actions({} as CachedFeatherBrief)[A.loading]({ state });

      expect(state.loading).toBe(true);
    });
  });

  describe(A.doneLoading, () => {
    it("should unmark loading", async () => {
      const state = { loading: true };
      // @ts-ignore
      await actions({})[A.doneLoading]({ state });

      expect(state.loading).toBe(false);
    });
  });

  describe(A.deleteFeed, () => {
    it("should remove the feed from the store", async () => {
      const feeds = FeedWithItemsArrayGenerator.n(2).generate();
      const toDelete = feeds[0].feedUrl;
      const ctx = {
        commit: jest.fn(),
        state: {
          feedsWithItems: feeds,
        },
      };
      // @ts-ignore
      await actions({})[A.deleteFeed](ctx, toDelete);
      expect(ctx.commit).toHaveBeenCalledWith(M.updateFeedWithItems, [
        feeds[1],
      ]);
    });
  });

  describe(A.didFindNewVersion, () => {
    it(`should call commit(${M.updateNewVersionFound}, true)`, async () => {
      const ctx = { commit: jest.fn() };
      // @ts-ignore
      await actions({})[A.didFindNewVersion](ctx);

      expect(ctx.commit).toHaveBeenCalledWith(M.updateNewVersionFound, true);
    });
  });

  describe(A.refreshUserFeeds, () => {
    it(`sould refresh user feeds`, async () => {
      const ctx = {
        dispatch: jest.fn(),
        commit: jest.fn(),
        state: {},
      };

      const $fb = ({
        getUserFeeds: jest.fn().mockResolvedValue([]),
      } as unknown) as CachedFeatherBrief;

      // @ts-ignore
      await actions($fb)[A.refreshUserFeeds](ctx);

      expect(ctx.dispatch).toHaveBeenCalledWith(A.loading);
      expect($fb.getUserFeeds).toHaveBeenCalled();
      expect(ctx.commit).toHaveBeenCalledWith(M.updateUserFeeds, []);
      expect(ctx.dispatch).toHaveBeenCalledWith(A.doneLoading);
    });
  });
});

describe("commits", () => {
  let feedsWithItems: FeedWithItems[];
  let state: FeatherBriefState;

  beforeEach(() => {
    feedsWithItems = FeedWithItemsArrayGenerator.n(2)
      .first()
      .withItems(2)
      .back()
      .nth(2)
      .withItems(2)
      .back()
      .generate();

    state = {
      feedsWithItems,
      deletedUrls: [],
      synced: true,
      newVersionFound: false,
      loading: false,
      userFeeds: [],
    };
  });

  describe(M.updateDeleted, () => {
    it("should should delete the whole feed if all its items are deleted", async () => {
      const urlsToDelete = feedsWithItems[0].items.map((f) => f.articleUrl);

      mutations[M.updateDeleted](state, urlsToDelete);

      const expectedState = {
        ...state,
        deletedUrls: urlsToDelete,
        feedsWithItems: [feedsWithItems[1]],
      };

      expect(state).toStrictEqual(expectedState);
    });
  });

  describe(M.updateStatus, () => {
    it("should update status of urls", () => {
      const urls = feedsWithItems[0].items.map((i) => i.articleUrl);
      mutations[M.updateStatus](state, {
        urls,
        status: ArticleStatus.SAVED,
        value: true,
      });

      const expected = state.feedsWithItems.map((feed, i) => {
        return i !== 0
          ? feed
          : {
              ...feed,
              items: feed.items.map((item) => ({ ...item, saved: true })),
            };
      });

      expect(state.feedsWithItems).toStrictEqual(expected);
    });

    it.each([ArticleStatus.DOWNVOTE, ArticleStatus.UPVOTE])(
      "should ensure only `%s` is `true`",
      (status) => {
        const feeds = [
          {
            ...feedsWithItems[0],
            items: feedsWithItems[0].items.map((i) => ({
              ...i,
              [status]: true,
            })),
          },
          feedsWithItems[1],
        ];
        state.feedsWithItems = feeds;

        const urls = feedsWithItems[0].items.map((item) => item.articleUrl);

        const expected = feeds.map((feed, i) => {
          return i === 0
            ? {
                ...feed,
                items: feed.items.map((i) => ({
                  ...i,
                  [status]: true,
                  [status === ArticleStatus.UPVOTE
                    ? ArticleStatus.DOWNVOTE
                    : ArticleStatus.UPVOTE]: false,
                })),
              }
            : feed;
        });

        mutations[M.updateStatus](state, {
          urls,
          status,
          value: true,
        });

        expect(state.feedsWithItems).toStrictEqual(expected);
      }
    );
  });

  describe(M.updateNewVersionFound, () => {
    it.each([true, false])("should mark 'newVersionFound' as '%s'", (found) => {
      state = {
        ...state,
        newVersionFound: !found,
      };

      mutations[M.updateNewVersionFound](state, found);

      expect(state.newVersionFound).toBe(found);
    });
  });

  describe(M.updateFeedCategory, () => {
    it("should update the feed items and userFeeds with the new category", () => {
      const feedWithItems = FeedWithItemsArrayGenerator.fromFeeds([
        {
          feedName: "Feed #1",
          feedUrl: "https://feed-1.io",
          category: DEFAULT_CATEGORY,
        },
        {
          feedName: "Feed #2",
          feedUrl: "https://feed-2.io",
          category: DEFAULT_CATEGORY,
        },
      ])
        .first()
        .withItems(4)
        .back()
        .nth(2)
        .withItems(2)
        .back()
        .generate();

      const state = {
        feedsWithItems,
        userFeeds: feedWithItems.map((f) => omit(f, "items")),
      } as FeatherBriefState;

      mutations[M.updateFeedCategory](state, {
        feedName: "Feed #1",
        feedUrl: "https://feed-1.io",
        category: "NEW_CATEGORY",
      });
      expect(
        state.userFeeds.find((f) => f.feedName === "Feed #1")
      ).toHaveProperty("category", "NEW_CATEGORY");
      expect(
        state.feedsWithItems
          .filter((f) => f.feedName === "Feed #1")
          .every((fi) => fi.category === "NEW_CATEGORY")
      ).toBe(true);
    });
  });
});

describe("getters", () => {
  describe(Getters.notShownNorSavedFeedItems, () => {
    it("should return not shown nor saved feeditems", () => {
      const state = {
        feedsWithItems: FeedWithItemsArrayGenerator.n(2)
          .first()
          .withItems(2)
          .first()
          .saved()
          .back()
          .back()
          .nth(2)
          .withItems(2)
          .first()
          .shown()
          .back()
          .back()
          .generate(),
      } as FeatherBriefState;

      // @ts-ignore
      const got: FeedWithItems[] = getters[Getters.notShownNorSavedFeedItems](
        state
      );

      expect(got).toHaveLength(2);
      expect(got.every((x) => x.items.every((y) => !y.saved && !y.shown))).toBe(
        true
      );
    });
  });

  describe(Getters.userFeedsNotInReadingList, () => {
    it("should return only feeds that are not in reading list", () => {
      const feedsWithItems = FeedWithItemsArrayGenerator.n(2)
        .first()
        .withItems(2)
        .back()
        .nth(2)
        .withItems(2)
        .back()
        .generate();

      const feed:
        | Feed
        | FeedWithItems = new FeedWithItemsGenerator().generate();
      // @ts-ignore
      delete feed.items;

      const state: FeatherBriefState = {
        feedsWithItems,
        userFeeds: feedsWithItems
          .map((f) => {
            // @ts-ignore
            delete f.items;
            return f as Feed;
          })
          .concat(feed),
      } as FeatherBriefState;

      // @ts-ignore
      const got = getters[Getters.userFeedsNotInReadingList](state);
      expect(got).toHaveLength(1);
      expect(got[0].feedUrl).toBe(feed.feedUrl);
    });
  });

  describe(Getters.itemsGroupedByCategory, () => {
    it("should group FeedItems by category", () => {
      const feedsWithItems = FeedWithItemsArrayGenerator.fromFeeds([
        {
          feedName: "Feed #1",
          feedUrl: "https://number-1-feed.io",
          category: "FirstCategory",
        },
        {
          feedName: "Feed #2",
          feedUrl: "https://number-2-feed.io",
          category: "SecondCategory",
        },
        {
          feedName: "Feed #3",
          feedUrl: "https://number-3-feed.io",
          category: "SecondCategory",
        },
      ])
        .first()
        .withItems(1)
        .back()
        .nth(2)
        .withItems(2)
        .back()
        .nth(3)
        .withItems(3)
        .back()
        .generate();

      const state = { feedsWithItems } as FeatherBriefState;

      // @ts-ignore
      const got: Record<string, FeedWithItems[]> = getters[
        Getters.itemsGroupedByCategory
      ](state);

      expect(got).toHaveProperty("FirstCategory");
      expect(got.FirstCategory).toHaveLength(1);

      expect(got).toHaveProperty("SecondCategory");
      expect(got.SecondCategory).toHaveLength(2);
      expect(
        got.SecondCategory.flatMap((f: FeedWithItems) => f.items)
      ).toHaveLength(5);
    });
  });

  describe(Getters.allCategories, () => {
    it("should provide all the categories", () => {
      const state = {
        userFeeds: [
          {
            feedName: "Feed #1",
            feedUrl: "https://number-1-feed.io",
            category: "FirstCategory",
          },
          {
            feedName: "Feed #2",
            feedUrl: "https://number-2-feed.io",
            category: "SecondCategory",
          },
          {
            feedName: "Feed #3",
            feedUrl: "https://number-3-feed.io",
            category: "SecondCategory",
          },
        ],
      } as FeatherBriefState;

      expect(
        // @ts-ignore
        getters[Getters.allCategories](state)
      ).toStrictEqual(["FirstCategory", "SecondCategory"]);
    });
  });
});
