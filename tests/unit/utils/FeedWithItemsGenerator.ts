import { Chance } from "chance";
import { FeedWithItems } from "@/domain/FeedWithItems";
import { DEFAULT_CATEGORY, FeedItem } from "@/domain/FeedItem";
import { Feed } from "@/domain/Feed";

interface Generator<T> {
  generate(): T;
}

interface Backable<T> {
  back(): T;
}

export class FeedWithItemsArrayGenerator implements Generator<FeedWithItems[]> {
  private feeds: FeedWithItemsGenerator[] = [];

  private constructor() {}

  generate(): FeedWithItems[] {
    return this.feeds.map((feed) => feed.generate());
  }

  static n(n: number) {
    const instance = new FeedWithItemsArrayGenerator();
    instance.feeds = Array.from({ length: n }).map(() => {
      return FeedWithItemsGenerator.from(instance);
    });

    return instance;
  }

  static fromFeeds(feeds: Feed[]) {
    const instance = new FeedWithItemsArrayGenerator();
    instance.feeds = feeds.map((feed) =>
      FeedWithItemsGenerator.from(
        instance,
        feed.feedName,
        feed.feedUrl,
        feed.category
      ).withItems(2)
    );

    return instance;
  }

  first() {
    if (this.feeds.length === 0) {
      throw new Error();
    }

    return this.feeds[0];
  }

  nth(n: number) {
    if (n > this.feeds.length) {
      throw new Error();
    }

    return this.feeds[n - 1];
  }
}

export class FeedWithItemsGenerator
  implements Generator<FeedWithItems>, Backable<FeedWithItemsArrayGenerator> {
  public feedWithItems: FeedWithItems;
  private items: FeedItemGenerator[] = [];
  backInstance?: FeedWithItemsArrayGenerator;

  constructor(
    feedName?: string,
    feedUrl?: string,
    category = DEFAULT_CATEGORY
  ) {
    const chance = new Chance();
    this.feedWithItems = {
      category,
      feedName: feedName ?? chance.sentence({ words: 5 }),
      feedUrl: feedUrl ?? chance.url(),
      items: [],
    };
  }

  static from(
    generator: FeedWithItemsArrayGenerator,
    feedName?: string,
    feedUrl?: string,
    category = DEFAULT_CATEGORY
  ) {
    const instance = new FeedWithItemsGenerator(feedName, feedUrl, category);
    instance.backInstance = generator;
    return instance;
  }

  generate() {
    this.feedWithItems.items = this.items.map((item) => item.generate());
    return this.feedWithItems;
  }

  withItems(n: number) {
    this.items = Array.from({ length: n }).map(() =>
      FeedItemGenerator.from(this)
    );

    return this;
  }

  first() {
    if (this.items.length === 0) {
      throw new Error();
    }

    return this.items[0];
  }

  back() {
    if (!this.backInstance) {
      throw new Error();
    }

    return this.backInstance;
  }
}

export class FeedItemGenerator
  implements Generator<FeedItem>, Backable<FeedWithItemsGenerator> {
  private base: FeedItem;
  private backInstance?: FeedWithItemsGenerator;

  constructor(
    feedName?: string,
    feedUrl?: string,
    category: string = DEFAULT_CATEGORY
  ) {
    const chance = new Chance();
    this.base = {
      feedName: feedName ?? chance.sentence({ words: 5 }),
      feedUrl: feedUrl ?? chance.url(),
      articleUrl: chance.url(),
      date: chance.date().getTime(),
      email: chance.email(),
      author: chance.name(),
      description: chance.sentence({ words: 10 }),
      summary: chance.sentence({ words: 10 }),
      title: chance.sentence(),
      category,
      saved: false,
      read: false,
      shown: false,
      downvote: false,
      upvote: false,
      score: chance.integer({ min: 0, max: 100 }),
      parsable: true,
    };
  }

  static from(back: FeedWithItemsGenerator) {
    const instance = new FeedItemGenerator(
      back.feedWithItems.feedName,
      back.feedWithItems.feedUrl,
      back.feedWithItems.category
    );
    instance.backInstance = back;

    return instance;
  }

  back() {
    if (!this.backInstance) {
      throw new Error();
    }

    return this.backInstance;
  }

  saved() {
    this.base.saved = true;
    return this;
  }

  read() {
    this.base.read = true;
    return this;
  }

  shown() {
    this.base.shown = true;
    return this;
  }

  notSaved() {
    this.base.saved = false;
    return this;
  }

  notRead() {
    this.base.read = false;
    return this;
  }

  notShown() {
    this.base.shown = false;
    return this;
  }

  articleUrl(url: string) {
    this.base.articleUrl = url;
    return this;
  }

  generate() {
    return this.base;
  }
}
