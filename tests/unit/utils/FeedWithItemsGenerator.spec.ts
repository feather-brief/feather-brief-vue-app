import {
  FeedWithItemsGenerator,
  FeedWithItemsArrayGenerator,
  FeedItemGenerator,
} from "./FeedWithItemsGenerator";
import { Feed } from "@/domain/Feed";

test("FeedItemGenerator", () => {
  const feedWithItems = new FeedWithItemsGenerator()
    .withItems(2)
    .first()
    .saved()
    .back()
    .generate();

  expect(feedWithItems.items).toHaveLength(2);
  expect(feedWithItems.items[0].saved).toBe(true);
});

test("FeedItemGenerator#notSaved", () => {
  const feedWithItems = new FeedWithItemsGenerator()
    .withItems(2)
    .first()
    .notSaved()
    .back()
    .generate();

  expect(feedWithItems.items).toHaveLength(2);
  expect(feedWithItems.items[0].saved).toBe(false);
});

test("#first without items should throw error", () => {
  function generate() {
    new FeedWithItemsGenerator("feedname", "feedurl").first();
  }

  expect(generate).toThrow();
});

describe("FeedItemGenerator", () => {
  function capitalize(word: string): string {
    return word[0].toUpperCase() + word.slice(1);
  }

  ["saved", "read", "shown"].forEach((attr) => {
    it(`#${attr}() should mark as ${attr}`, () => {
      // @ts-ignore
      expect(new FeedItemGenerator()[attr]().generate()[attr]).toBe(true);
    });

    it(`#not${capitalize(attr)}() should define ${attr} as false`, () => {
      expect(
        // @ts-ignore
        new FeedItemGenerator()[`not${capitalize(attr)}`]().generate()[attr]
      ).toBe(false);
    });
  });

  test("#articleUrl() should define the articleUrl", () => {
    expect(
      new FeedItemGenerator().articleUrl("url").generate().articleUrl
    ).toBe("url");
  });

  test("#back() should throw if back is undefined", () => {
    expect(() => new FeedItemGenerator().back()).toThrow();
  });
});

describe("FeedWithItemsGenerator", () => {
  it("#back() should throw if no back is defined", () => {
    expect(() => new FeedWithItemsGenerator().back()).toThrow();
  });
});

describe("FeedWithItemsArrayGenerator", () => {
  test("n", () => {
    const feedWithItems = FeedWithItemsArrayGenerator.n(2).generate();
    expect(feedWithItems).toHaveLength(2);
  });

  test("fromFeeds", () => {
    const stub: Feed = { feedName: "feedName", feedUrl: "feedUrl" };
    const feedWithItems = FeedWithItemsArrayGenerator.fromFeeds([
      stub,
    ]).generate();

    expect(feedWithItems).toHaveLength(1);
    expect(feedWithItems[0].feedUrl).toBe(stub.feedUrl);
    expect(feedWithItems[0].feedName).toBe(stub.feedName);

    expect(feedWithItems[0].items).toHaveLength(2);
    expect(feedWithItems[0].items[0].feedUrl).toBe(stub.feedUrl);
    expect(feedWithItems[0].items[0].feedName).toBe(stub.feedName);
  });

  test("first", () => {
    const feeds = FeedWithItemsArrayGenerator.n(2)
      .first()
      .withItems(1)
      .first()
      .saved()
      .read()
      .back()
      .back()
      .generate();

    expect(feeds).toHaveLength(2);
    expect(feeds[0].items[0].saved).toBe(true);
    expect(feeds[0].items[0].read).toBe(true);
  });

  test("#first() without items should throw", () => {
    const generate = () => FeedWithItemsArrayGenerator.fromFeeds([]).first();

    expect(generate).toThrow();
  });

  test("#nth()", () => {
    expect(
      FeedWithItemsArrayGenerator.fromFeeds([
        { feedName: "", feedUrl: "" },
        { feedName: "feedName", feedUrl: "feedUrl" },
      ])
        .nth(2)
        .generate()
    ).toHaveProperty("feedName", "feedName");
  });

  test("#nth() throws when > than the number of items", () => {
    expect(() =>
      FeedWithItemsArrayGenerator.fromFeeds([
        { feedName: "", feedUrl: "" },
      ]).nth(2)
    ).toThrow();
  });
});
