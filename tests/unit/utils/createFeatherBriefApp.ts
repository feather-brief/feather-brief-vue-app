import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import Vuetify from "vuetify";
import { createLocalVue } from "@vue/test-utils";
import { useAuth0, Auth0Plugin } from "@/plugins/auth/fakeAuth";
import { CachedFeatherBrief } from "@/plugins/feather-brief/CachedFeatherBrief";
import { FeatherBrief } from "@/plugins/feather-brief/FeatherBrief";
import { HttpService } from "@/plugins/feather-brief/HttpService";
import router from "@/plugins/router/router";
import { useStore } from "@/store/useStore";
import { Actions } from "@/store/Actions";

export const BASE_URL = "https://example.com";

Vue.use(Vuetify);
const vuetify = new Vuetify();

export async function createFeatherBriefApp({ shouldDispatch = true } = {}) {
  const $auth = useAuth0();
  const $fb = CachedFeatherBrief.from(
    new FeatherBrief($auth, new HttpService(BASE_URL))
  );
  const localVue = createLocalVue();
  localVue.use(Vuex);
  localVue.use(VueRouter);
  localVue.use(Auth0Plugin);
  localVue.use({
    install(vue) {
      vue.prototype.$fb = $fb;
    },
  });

  const store = await useStore($fb);

  if (shouldDispatch) {
    await store.dispatch(Actions.refreshFeedItems);
  }

  return {
    localVue,
    store,
    router,
    vuetify,
  };
}
