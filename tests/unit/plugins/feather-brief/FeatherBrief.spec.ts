import { AuthService } from "@/types/Auth0VuePlugin";
import { HttpService } from "@/plugins/feather-brief/HttpService";
import { FeatherBriefArticleParsed } from "@/plugins/feather-brief/IFeatherBrief";
import {
  FeatherBrief,
  FeatherBriefRestInterface,
} from "@/plugins/feather-brief/FeatherBrief";
import { FeedWithItems } from "@/domain/FeedWithItems";
import { FeedItem } from "@/domain/FeedItem";
import { ArticleStatus } from "@/domain/ArticleStatus";
import {
  FeedWithItemsArrayGenerator,
  FeedWithItemsGenerator,
} from "../../utils/FeedWithItemsGenerator";

const $auth: AuthService = ({
  getIdTokenClaims: jest.fn(() => Promise.resolve({ __raw: "1234" })),
  user: {
    email: "sarah.jones@mail.com",
  },
} as Partial<AuthService>) as AuthService;

const $http: HttpService = ({
  GET: jest.fn(),
  POST: jest.fn(),
  PUT: jest.fn(),
  PATCH: jest.fn(),
  DELETE: jest.fn(),
} as Partial<HttpService>) as HttpService;

$http.with = jest.fn(() => $http);

function fbFactory() {
  return new FeatherBrief($auth, $http);
}

describe("getLastFeedItems", () => {
  beforeEach(() => {
    // @ts-ignore
    $http.with.mockClear();
    // @ts-ignore
    $auth.getIdTokenClaims.mockClear();
  });

  afterAll(() => {
    $http.GET = jest.fn();
    // @ts-ignore
    $http.with.mockClear();
    // @ts-ignore
    $auth.getIdTokenClaims.mockClear();
  });

  it("should be authenticated", async () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      json() {
        return Promise.resolve({
          items: [],
        });
      },
    });
    await fbFactory().getLastFeedItems();
    expect($auth.getIdTokenClaims).toHaveBeenCalled();
    expect($http.with).toHaveBeenCalled();
    expect($http.GET).toHaveBeenCalled();
  });

  it("should throw if the status code is not ok", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({ ok: false });

    return expect(fbFactory().getLastFeedItems()).rejects.toThrow();
  });

  it("should throw if root key is not items", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      async json() {
        return [];
      },
    });

    return expect(fbFactory().getLastFeedItems()).rejects.toThrow();
  });

  it("should throw if root key 'items' is not an array", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      async json() {
        return { items: {} };
      },
    });

    return expect(fbFactory().getLastFeedItems()).rejects.toThrow();
  });

  it("should deduplicate the urls in feeditems", () => {
    const items = [
      {
        feedName: "Feed Name",
        feedUrl: "http://feed.tld",
        articleUrl: "http://article.tld",
      },
      {
        feedName: "Feed Name",
        feedUrl: "http://feed.tld",
        articleUrl: "http://article.tld",
      },
    ];

    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      json() {
        return Promise.resolve({
          items,
        });
      },
    });

    return expect(fbFactory().getLastFeedItems()).resolves.toEqual([
      {
        feedName: "Feed Name",
        feedUrl: "http://feed.tld",
        items: [
          {
            feedName: "Feed Name",
            feedUrl: "http://feed.tld",
            articleUrl: "http://article.tld",
          },
        ],
      },
    ]);
  });

  it("should group feeditems by feeds", () => {
    const feedWithItems: FeedWithItems[] = FeedWithItemsArrayGenerator.fromFeeds(
      [
        {
          feedName: "First Feed",
          feedUrl: "https://first-feed.tld",
        },
        {
          feedName: "Second Feed",
          feedUrl: "http://second-feed.tld",
        },
      ]
    )
      .first()
      .withItems(3)
      .back()
      .nth(2)
      .withItems(3)
      .back()
      .generate();

    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      json() {
        return Promise.resolve({
          items: feedWithItems.flatMap((f) => f.items),
        });
      },
    });

    return expect(fbFactory().getLastFeedItems()).resolves.toEqual(
      feedWithItems
    );
  });

  it("should handle an unauthenticated user", () => {
    // @ts-ignore
    $auth.getIdTokenClaims.mockReturnValueOnce(Promise.resolve(undefined));

    return expect(fbFactory().getLastFeedItems()).resolves.toEqual([]);
  });

  it("should handle server error", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      json() {
        return Promise.resolve({
          error: "There was an error",
        });
      },
    });

    return expect(fbFactory().getLastFeedItems()).rejects.toThrow();
  });
});

describe("createFeed", () => {
  afterAll(() => {
    // @ts-ignore
    $http.GET.mockClear();
    // @ts-ignore
    $http.POST.mockClear();
    // @ts-ignore
    $http.with.mockClear();
  });

  it("should use POST", async () => {
    // @ts-ignore
    $http.POST.mockReturnValue((feed) =>
      Promise.resolve({
        ok: true,
        status: 200,
        json() {
          return Promise.resolve(feed);
        },
      })
    );

    await fbFactory().createFeed({
      feedName: "FeedName",
      feedUrl: "http://feed-url.tld",
    });

    expect($http.POST).toHaveBeenCalledWith(FeatherBriefRestInterface.FEEDS, {
      email: $auth.user?.email,
      name: "FeedName",
      url: "http://feed-url.tld",
    });
  });

  it("should throw DuplicateFeedError on 304 status", async () => {
    // @ts-ignore
    $http.POST.mockReturnValue(
      Promise.resolve({
        ok: false,
        status: 304,
      })
    );

    try {
      await fbFactory().createFeed({ feedName: "feedName", feedUrl: "url" });
    } catch (e) {
      expect(e.message).toBe("DuplicateFeedError");
    }
  });

  it("should throw CantProcessFeedError on 500 status", async () => {
    // @ts-ignore
    $http.POST.mockReturnValue(
      Promise.resolve({
        ok: false,
        status: 500,
      })
    );

    try {
      await fbFactory().createFeed({ feedName: "feedName", feedUrl: "url" });
    } catch (e) {
      expect(e.message).toBe("CantProcessFeedError");
    }
  });

  it("should throw if no feedUrl is provided", async () => {
    try {
      await fbFactory().createFeed({ feedName: "feedName" });
    } catch (e) {
      expect(e.message).toBe("Required the feedUrl");
    }
  });
});

describe("parseUrl", () => {
  afterAll(() => {
    // @ts-ignore
    $http.GET.mockClear();
  });

  afterEach(() => {
    // @ts-ignore
    $http.GET.mockClear();
    // @ts-ignore
    $http.with.mockClear();
  });

  it("should throw if the provided url is not a valid url", async () => {
    try {
      await fbFactory().parseUrl("invalid Url");
    } catch (e) {
      expect(e.message).toBe(
        "the provided url is not a valid URL: 'invalid Url'"
      );
    }
  });

  it("should throw if the status is not ok", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: false,
    });

    return expect(fbFactory().parseUrl("http://url.tld")).rejects.toThrow();
  });

  it("should make a GET request", async () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      json() {
        return Promise.resolve({
          title: "title",
          content: "content",
        } as FeatherBriefArticleParsed);
      },
    });

    await fbFactory().parseUrl("http://article.com");

    expect($http.GET).toHaveBeenCalled();
    expect($http.with).not.toHaveBeenCalled();
  });

  it("should throw if no content is available in response", async () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      json() {
        return Promise.resolve({
          title: "title",
        } as FeatherBriefArticleParsed);
      },
    });

    try {
      await fbFactory().parseUrl("http://article.com");
    } catch (e) {
      expect(e.message).toBe("CantParseArticle");
    }
  });

  it("should replace every relative link by absolute links", async () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      json() {
        return Promise.resolve({
          title: "title",
          content: `<a href="hello.html">See Hello</a>`,
        } as FeatherBriefArticleParsed);
      },
    });

    const parsed = await fbFactory().parseUrl("https://article.tld/article");

    expect(parsed.content).toBe(
      `<a href="https://article.tld/hello.html">See Hello</a>`
    );
  });

  it("should replace every relative image by absolute image URL", async () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      json() {
        return Promise.resolve({
          title: "title",
          content: `<img src="/logo.png">`,
        } as FeatherBriefArticleParsed);
      },
    });

    const parsed = await fbFactory().parseUrl(
      "https://article.tld/articles/2020-04-01-no-jokes.html"
    );

    expect(parsed.content).toBe(`<img src="https://article.tld/logo.png">`);
  });

  it("should remove every `width` and `height` property of `img`s", async () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      json() {
        return Promise.resolve({
          title: "title",
          content: `<img width="150" height="150" src="/logo.png">`,
        } as FeatherBriefArticleParsed);
      },
    });

    const parsed = await fbFactory().parseUrl(
      "https://article.tld/articles/2020-04-01-no-jokes.html"
    );

    expect(parsed.content).toBe(`<img src="https://article.tld/logo.png">`);
  });
});

describe.each(["mark", "unmark"] as const)(`%s()`, (update) => {
  const init = () => {
    // @ts-ignore
    $auth.getIdTokenClaims.mockClear();
    // @ts-ignore
    $http.with.mockClear();
    // @ts-ignore
    $http.PUT.mockClear();
  };

  afterAll(init);

  beforeEach(() => {
    init();
    // @ts-ignore
    $http.with.mockReturnValue($http);
    // @ts-ignore
    $http.PUT.mockResolvedValue({
      ok: true,
      json() {
        Promise.resolve();
      },
    });
  });

  it("should be authenticated", async () => {
    await fbFactory()[update]("http://article.tld", ArticleStatus.SAVED);

    expect($auth.getIdTokenClaims).toHaveBeenCalled();
    expect($http.with).toHaveBeenCalled();
  });

  it("should mock $http correctly", () => {
    // Smoke test
    expect($http.with("abc")).toBe($http);
  });

  it("should use a single articleUrl", async () => {
    // FAILS...
    await fbFactory()[update]("http://article.tld", ArticleStatus.SAVED);

    expect($http.with("abc").PUT).toHaveBeenCalled();
  });

  it("should use a URL array", async () => {
    // FAILS...
    await fbFactory()[update](
      ["http://article.tld", "http://article-2.tld"],
      ArticleStatus.SAVED
    );

    expect($http.PUT).toHaveBeenCalledWith(expect.stringMatching(/.+/), {
      articles: ["http://article.tld", "http://article-2.tld"],
    });
  });

  it("should call PUT", async () => {
    // FAILS...
    await fbFactory()[update]("http://article.tld", ArticleStatus.SAVED);

    expect($http.PUT).toHaveBeenCalledWith(
      `${FeatherBriefRestInterface.FEED_ITEMS}?${update}As=${ArticleStatus.SAVED}`,
      {
        articles: ["http://article.tld"],
      }
    );
  });
});

describe("downloadArticle", () => {
  it("should throw", () => {
    return expect(
      fbFactory().downloadArticle({} as FeedItem)
    ).rejects.toStrictEqual(new Error("Should not be called"));
  });
});

describe("offloadArticle", () => {
  it("should throw", () => {
    return expect(
      fbFactory().offloadArticle({} as FeedItem)
    ).rejects.toStrictEqual(new Error("should not be called"));
  });
});

describe("exploreFeeds", () => {
  beforeEach(() => {
    // @ts-ignore
    $http.GET.mockClear();
  });

  afterAll(() => {
    // @ts-ignore
    $http.GET.mockClear();
  });

  it("should throw if no 'feeds' key is available in response", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      async json() {
        // Missing "feeds" keys
        return [{ feedName: "feedName", feedUrl: "feedUrl" }];
      },
    });

    return expect(fbFactory().exploreFeeds()).rejects.toThrowError(
      "Could not explore feeds correctly"
    );
  });

  it("should throw if the result is not an array", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      async json() {
        return {
          feeds: { feedName: "feedName", feedUrl: "feedUrl" },
        };
      },
    });

    return expect(fbFactory().exploreFeeds()).rejects.toThrowError(
      "Could not explore feeds correctly"
    );
  });

  it("should use GET", async () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      status: 200,
      ok: true,
      async json() {
        return {
          feeds: [{ feedName: "feedName", feedUrl: "feedUrl" }],
        };
      },
    });

    await fbFactory().exploreFeeds();
    expect($http.GET).toHaveBeenCalledWith(
      FeatherBriefRestInterface.EXPLORE_FEEDS
    );
  });

  it("should map name and string record to Feed[]", () => {
    // @ts-ignore
    $http.GET.mockResolvedValue({
      json() {
        return Promise.resolve({
          feeds: [
            { name: "Feed#1", url: "http://feed-nb-1.tld" },
            { name: "Feed#2", url: "http://feed-nb-2.tld" },
          ],
        });
      },
      status: 200,
      ok: true,
    });

    expect(fbFactory().exploreFeeds()).resolves.toStrictEqual([
      { feedName: "Feed#1", feedUrl: "http://feed-nb-1.tld" },
      { feedName: "Feed#2", feedUrl: "http://feed-nb-2.tld" },
    ]);
  });
});

describe("uploadOpml", () => {
  const file = new File([new Blob()], "filename");
  beforeEach(() => {
    // @ts-ignore
    $http.PUT.mockClear();
  });

  afterAll(() => {
    // @ts-ignore
    $http.PUT.mockClear();
  });

  /* TODO */
  it("should use PUT", async () => {
    // @ts-ignore
    $http.PUT.mockReturnValue(
      Promise.resolve({
        ok: true,
        status: 200,
        async json() {
          return {};
        },
      })
    );

    const fd = new FormData();
    fd.append("opml", file);

    await fbFactory().uploadOpml(file);
    expect($http.PUT).toHaveBeenCalledWith(FeatherBriefRestInterface.OPML, fd);
  });

  it("should throw if there is a failure during upload", async () => {
    // @ts-ignore
    $http.PUT.mockReturnValue({
      ok: false,
    });

    expect(fbFactory().uploadOpml(file)).rejects.toThrow();
  });
});

describe("getSavedFeedItems", () => {
  const init = () => {
    // @ts-ignore
    $auth.getIdTokenClaims.mockClear();
    // @ts-ignore
    $auth.getIdTokenClaims.mockResolvedValue({
      __raw: "1234",
    });
    // @ts-ignore
    $http.GET.mockClear();
  };

  beforeEach(() => {
    init();
    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      status: 200,
      async json() {
        return { items: [] };
      },
    });
  });
  afterAll(init);

  it("should call FEED_ITEMS with a filter", async () => {
    expect.assertions(2);

    $http.GET = jest.fn((url) => {
      const query = new URLSearchParams(url.split("?")[1]);
      expect(query.get("size")).toBe("25");
      expect(query.get("filter")).toBe(ArticleStatus.SAVED);

      // @ts-ignore
      return Promise.resolve({
        ok: true,
        status: 200,
        async json() {
          return { items: [] };
        },
      });
    });

    await fbFactory().getSavedFeedItems({ size: 25 });
  });

  it("should use 100 as default size", async () => {
    await fbFactory().getSavedFeedItems();

    expect($http.GET).toHaveBeenCalledWith(expect.stringMatching(/size=100/));
  });

  it("should handle a non-authenticated user", () => {
    // @ts-ignore
    $auth.getIdTokenClaims.mockResolvedValueOnce(undefined);

    expect(fbFactory().getSavedFeedItems()).resolves.toEqual([]);
  });
});

describe("getUserFeeds", () => {
  afterAll(() => {
    // @ts-ignore
    $http.GET.mockReset();

    // @ts-ignore
    $auth.getIdTokenClaims.mockClear();
  });

  it("should return user feeds", () => {
    const feed = new FeedWithItemsGenerator();
    // @ts-ignore
    $http.GET.mockImplementation((url) => {
      if (url === FeatherBriefRestInterface.FEEDS) {
        return Promise.resolve({
          ok: true,
          status: 200,
          json: () =>
            Promise.resolve({
              feeds: [feed],
            }),
        });
      }

      return Promise.resolve(undefined);
    });

    return expect(fbFactory().getUserFeeds()).resolves.toEqual([feed]);
  });

  it("should handle unauthenticated user", () => {
    // @ts-ignore;
    $auth.getIdTokenClaims.mockResolvedValueOnce(undefined);

    return expect(fbFactory().getUserFeeds()).resolves.toEqual([]);
  });

  it("should rejects when is status is non 200", () => {
    // @ts-ignore
    $http.GET.mockImplementation((url) => {
      if (url === FeatherBriefRestInterface.FEEDS) {
        return Promise.resolve({
          ok: false,
          status: 500,
        });
      }

      return Promise.resolve(undefined);
    });

    expect(fbFactory().getUserFeeds()).rejects.toThrow();
  });

  it("should rejects if the result changed schema", () => {
    // @ts-ignore
    $http.GET.mockImplementation((url) => {
      if (url === FeatherBriefRestInterface.FEEDS) {
        return Promise.resolve({
          ok: true,
          status: 200,
          json() {
            return Promise.resolve({
              items: [],
            });
          },
        });
      }

      return Promise.resolve(undefined);
    });

    expect(fbFactory().getUserFeeds()).rejects.toThrow();
  });

  it("should rejects if the result not an array", () => {
    // @ts-ignore
    $http.GET.mockImplementation((url) => {
      if (url === FeatherBriefRestInterface.FEEDS) {
        return Promise.resolve({
          ok: true,
          status: 200,
          json() {
            return Promise.resolve({
              feeds: { id: 0 },
            });
          },
        });
      }

      return Promise.resolve(undefined);
    });

    expect(fbFactory().getUserFeeds()).rejects.toThrow();
  });
});

describe("getFeedItems", () => {
  const init = () => {
    // @ts-ignore
    $http.with.mockReset();
    // @ts-ignore
    $http.with.mockReturnThis();
    // @ts-ignore
    $http.GET.mockReset();
  };

  beforeEach(() => {
    init();
    // @ts-ignore
    $http.GET.mockResolvedValue({
      ok: true,
      async json() {
        return { items: [] };
      },
    });
  });

  afterAll(init);

  it("should not have a size", async () => {
    await fbFactory().getFeedItems("feed");

    expect($http.GET).toHaveBeenCalledWith(
      expect.not.stringMatching(/size=.+/)
    );
  });
});

describe("unfollowFeed", () => {
  beforeEach(() => {
    // @ts-ignore
    $http.DELETE.mockResolvedValue({
      ok: true,
    });
  });

  afterAll(() => {
    // @ts-ignore
    $http.DELETE.mockReset();
  });

  it("should call DELETE", async () => {
    await fbFactory().unfollowFeed("https://feed.tld");
    expect($http.DELETE).toHaveBeenCalledWith(FeatherBriefRestInterface.FEEDS, {
      url: "https://feed.tld",
    });
  });
});
