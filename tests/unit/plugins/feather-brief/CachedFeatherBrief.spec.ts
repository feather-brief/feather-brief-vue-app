import { ArticleStatus } from "@/domain/ArticleStatus";
import { FeedWithItems } from "@/domain/FeedWithItems";
import { CachedFeatherBrief } from "@/plugins/feather-brief/CachedFeatherBrief";
import {
  IFeatherBrief,
  FeatherBriefArticleParsed,
} from "@/plugins/feather-brief/IFeatherBrief";
import {
  FeedWithItemsGenerator,
  FeedWithItemsArrayGenerator,
} from "../../utils/FeedWithItemsGenerator";
import { FeedItem } from "@/domain/FeedItem";

/**
 * The static nature of CachedFeatherBrief is a PITA
 * to test !!!
 */

const fakeFeatherBrief: Partial<IFeatherBrief> = {
  mark: jest.fn(),
  unmark: jest.fn(),
  parseUrl: jest.fn(),
  getLastFeedItems: jest.fn(),
  getSavedFeedItems: jest.fn(),
};

function fbFactory(feedWithItems: FeedWithItems[]) {
  // @ts-ignore
  fakeFeatherBrief.getLastFeedItems?.mockReturnValue(
    Promise.resolve(feedWithItems)
  );

  const instance = CachedFeatherBrief.from(fakeFeatherBrief as IFeatherBrief);
  instance.invalidate();

  return instance;
}

describe("marking/unmarking is only calling the network layer", () => {
  afterEach(() => {
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    localStorage.clear();
  });

  test("unmarking articles as saved should call FeatherBrief.unmark", async () => {
    const fakeFeedWithItems: FeedWithItems = new FeedWithItemsGenerator()
      .withItems(1)
      .first()
      .saved()
      .back()
      .generate();

    const $fb = fbFactory([fakeFeedWithItems]);

    // update the cache internally
    await $fb.unmark(
      fakeFeedWithItems.items[0].articleUrl,
      ArticleStatus.SAVED
    );

    expect(fakeFeatherBrief.unmark).toHaveBeenCalledWith(
      [fakeFeedWithItems.items[0].articleUrl],
      ArticleStatus.SAVED
    );
  });

  test("marking articles as saved should call FeatherBrief.mark", async () => {
    const fakeFeedWithItems: FeedWithItems = new FeedWithItemsGenerator()
      .withItems(1)
      .first()
      .notSaved()
      .back()
      .generate();

    const $fb = fbFactory([fakeFeedWithItems]);
    await $fb.mark(fakeFeedWithItems.items[0].articleUrl, ArticleStatus.SAVED);

    expect(fakeFeatherBrief.mark).toHaveBeenCalledWith(
      [fakeFeedWithItems.items[0].articleUrl],
      ArticleStatus.SAVED
    );
  });
});

describe("getLastFeedItems", () => {
  let $fb: CachedFeatherBrief;
  const feeditems = FeedWithItemsArrayGenerator.fromFeeds([
    {
      feedName: "FeedName",
      feedUrl: "http://feed.tld/url",
    },
  ]).generate();

  beforeAll(() => {
    $fb = fbFactory(feeditems);
  });

  afterAll(() => {
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    localStorage.clear();
  });

  it("should return the feedItems from the origin FeatherBrief", async () => {
    const remote = await $fb.getLastFeedItems();
    expect(remote).toEqual(feeditems);
    expect(fakeFeatherBrief.getLastFeedItems).toHaveBeenCalled();
  });
});

describe("parseUrl", () => {
  jest.useFakeTimers();

  let $fb: CachedFeatherBrief;
  beforeAll(() => {
    $fb = fbFactory([]);
    // @ts-ignore
    fakeFeatherBrief.parseUrl.mockReturnValue(
      Promise.resolve({
        title: "title",
        byline: "",
        content: "This is the article content",
        length: "This is the article content".length,
        excerpt: "",
        siteName: "",
      })
    );
  });

  afterAll(() => {
    $fb.invalidate();
    fakeFeatherBrief.parseUrl = jest.fn();
    // @ts-ignore
    CachedFeatherBrief.instance = null;

    jest.runAllTimers();
    localStorage.clear();
  });

  it("should parse a URL", async () => {
    await $fb.parseUrl("url");

    expect(fakeFeatherBrief.parseUrl).toHaveBeenCalled();
  });
});

describe("parseUrl with feedData", () => {
  let $fb: CachedFeatherBrief;

  beforeAll(() => {
    jest.useFakeTimers();

    // @ts-ignore
    fakeFeatherBrief.parseUrl.mockReturnValue(Promise.resolve({}));

    $fb = fbFactory([]);
  });

  afterAll(() => {
    jest.runAllTimers();

    fakeFeatherBrief.parseUrl = jest.fn();

    $fb.invalidate();
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    jest.useRealTimers();
  });

  it("should use the data from feedData", async () => {
    // @ts-ignore
    fakeFeatherBrief.parseUrl.mockReturnValue(Promise.resolve({}));

    const parsed = await $fb.parseUrl(
      "url",
      FeedWithItemsArrayGenerator.fromFeeds([
        { feedName: "feedName", feedUrl: "feedUrl" },
      ])
        .first()
        .withItems(1)
        .first()
        .articleUrl("url")
        .read()
        .shown()
        .back()
        .back()
        .generate()
    );

    expect(parsed).toMatchObject({
      saved: false,
      read: true,
      shown: true,

      feedName: "feedName",
      feedUrl: "feedUrl",
    } as FeatherBriefArticleParsed);

    fakeFeatherBrief.parseUrl = jest.fn();
  });
});

describe("orderByAttributes", () => {
  it("should return unread first", () => {
    const feeditems = new FeedWithItemsGenerator("FeedName", "feedUrl")
      .withItems(2)
      .first()
      .read()
      .back()
      .generate();

    // @ts-ignore
    const [ordered] = new CachedFeatherBrief({}).orderByAttributes([feeditems]);

    expect(ordered.items[0].read).toBe(false);
    expect(ordered.items[1].read).toBe(true);
  });

  it("should return in the initial order if none is read", () => {
    const feeditems = new FeedWithItemsGenerator("FeedName", "feedUrl")
      .withItems(2)
      .generate();

    // @ts-ignore
    const [ordered] = new CachedFeatherBrief({}).orderByAttributes([feeditems]);

    expect(ordered.items).toEqual(feeditems.items);
  });
});

describe("downloadArticle/offloadArticle", () => {
  let $fb: CachedFeatherBrief;
  const feeditems: FeedWithItems = new FeedWithItemsGenerator()
    .withItems(1)
    .first()
    .notSaved()
    .back()
    .generate();

  beforeAll(async () => {
    $fb = fbFactory([feeditems]);
    jest.useFakeTimers();

    // init the cache.
    await $fb.getLastFeedItems();

    // @ts-ignore
    fakeFeatherBrief.parseUrl.mockReturnValue(() => {
      jest.runAllTimers();
      return Promise.resolve({});
    });
  });

  afterAll(() => {
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    jest.runAllTimers();
    localStorage.clear();
    jest.useRealTimers();
  });

  it("should mark article as saved", async () => {
    const url = feeditems.items[0].articleUrl;
    await $fb.downloadArticle(feeditems.items[0]);

    expect(fakeFeatherBrief.mark).toHaveBeenCalledWith(
      [url],
      ArticleStatus.SAVED
    );
    expect(fakeFeatherBrief.parseUrl).toHaveBeenCalled();

    expect(
      // @ts-ignore
      $fb.isOfflineArticle(url)
    ).toBeTruthy();
  });

  it("should not call the origin layer when the article downloaded", async () => {
    // @ts-ignore
    fakeFeatherBrief.parseUrl.mockClear();

    await $fb.parseUrl(feeditems.items[0].articleUrl);
    expect(fakeFeatherBrief.parseUrl).not.toHaveBeenCalled();
  });

  it("should unmark article as saved", async () => {
    // @ts-ignore
    fakeFeatherBrief.unmark.mockClear();
    const url = feeditems.items[0].articleUrl;
    await $fb.offloadArticle(feeditems.items[0]);
    jest.runAllTimers();

    expect(fakeFeatherBrief.unmark).toHaveBeenCalledWith(
      [url],
      ArticleStatus.SAVED
    );

    // @ts-ignore
    expect($fb.isOfflineArticle(url)).toBe(false);
  });

  it("should do nothing if url is not in cache (has already been offloaded)", async () => {
    // @ts-ignore
    fakeFeatherBrief.unmark.mockClear();
    const url = feeditems.items[0].articleUrl;
    await $fb.offloadArticle(feeditems.items[0]);

    expect(fakeFeatherBrief.unmark).not.toHaveBeenCalled();
  });
});

describe("getSavedFeedItems", () => {
  beforeAll(() => {
    jest.useFakeTimers();
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    jest.runAllTimers();
    localStorage.clear();

    // We need to
    // @ts-ignore
    Object.keys(CachedFeatherBrief.offlineArticles).forEach((k) => {
      // @ts-ignore
      delete CachedFeatherBrief.offlineArticles[k];
    });
  });

  afterAll(() => {
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    localStorage.clear();
    jest.useRealTimers();
  });

  it("should return all saved items from cache", async () => {
    const feedWithItems: FeedWithItems = new FeedWithItemsGenerator()
      .withItems(2)
      .first()
      .saved()
      .back()
      .generate();

    const $fb = fbFactory([feedWithItems]);

    // We populate the cache
    await $fb.getLastFeedItems();

    const saved = await $fb.getSavedFeedItems();

    expect(saved).toHaveLength(1);
    expect(saved[0].articleUrl).toBe(feedWithItems.items[0].articleUrl);
  });
});

describe("installSavedFeedItems", () => {
  beforeAll(() => {
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    localStorage.clear();
    jest.useFakeTimers();
  });

  afterAll(() => {
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    localStorage.clear();

    fakeFeatherBrief.getSavedFeedItems = jest.fn();
    fakeFeatherBrief.parseUrl = jest.fn();
  });

  afterEach(() => {
    // @ts-ignore
    CachedFeatherBrief.instance = null;
    localStorage.clear();
  });

  it("should install all saved items", async () => {
    // @ts-ignore
    fakeFeatherBrief.parseUrl.mockClear();
    // @ts-ignore
    fakeFeatherBrief.getSavedFeedItems = jest.fn(() => {
      jest.runAllTimers();
      return Promise.resolve(
        new FeedWithItemsGenerator("feedName", "FeedUrl")
          .withItems(1)
          .first()
          .saved()
          .back()
          .generate().items
      );
    });

    const feedItemsFromRemote = FeedWithItemsArrayGenerator.n(1)
      .first()
      .withItems(2)
      .back()
      .generate();
    const $fb: CachedFeatherBrief = fbFactory(feedItemsFromRemote);

    await $fb.installSavedFeedItems();

    expect(fakeFeatherBrief.getSavedFeedItems).toHaveBeenCalledTimes(1);
    expect(fakeFeatherBrief.parseUrl).toHaveBeenCalledTimes(1);
  });

  it("should not install already parsed urls", async () => {
    const savedFeedItems: FeedItem[] = new FeedWithItemsGenerator(
      "feedName",
      "FeedUrl"
    )
      .withItems(1)
      .first()
      .saved()
      .back()
      .generate().items;

    // @ts-ignore
    fakeFeatherBrief.getSavedFeedItems = jest.fn(() => {
      jest.runAllTimers();
      return Promise.resolve(savedFeedItems);
    });

    // @ts-ignore
    fakeFeatherBrief.parseUrl = jest.fn(() => {
      jest.runAllTimers();
      return Promise.resolve({});
    });

    const feedItemsFromRemote = FeedWithItemsArrayGenerator.n(1)
      .first()
      .withItems(2)
      .back()
      .generate();

    const $fb: CachedFeatherBrief = fbFactory(feedItemsFromRemote);
    await $fb.downloadArticle(savedFeedItems[0]);
    // @ts-ignore
    fakeFeatherBrief.parseUrl.mockClear();

    await $fb.installSavedFeedItems();

    expect(fakeFeatherBrief.getSavedFeedItems).toHaveBeenCalled();
    expect(fakeFeatherBrief.parseUrl).not.toHaveBeenCalled();
  });
});
