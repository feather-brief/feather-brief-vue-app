window.console = ({
  debug: jest.fn(),
  warn: jest.fn(),
  info: jest.fn(),
  log: jest.fn(),
  error: jest.fn(),
} as unknown) as Console;

import { logger, LogLevel } from "@/domain/utils/logger";

let nodeEnv: any;
beforeAll(() => {
  nodeEnv = process.env.NODE_ENV;
  process.env.NODE_ENV = "development";
});

afterAll(() => {
  process.env.NODE_ENV = nodeEnv;
});

describe.each(["info", "debug", "error", "warn"] as const)(
  `console.%s`,
  (f) => {
    afterEach(() => {
      // @ts-ignore
      window.console[f].mockClear();
    });

    it(`should use a simple string as namespace`, () => {
      const l = logger("Namespace");
      l[f]("hello %s", "world");

      expect(window.console[f]).toHaveBeenCalledWith(
        "[Namespace] hello %s",
        "world"
      );
    });

    it(`should use a function name as namespace`, () => {
      function namespace() {}
      const l = logger(namespace);
      l[f]("hello");
      expect(window.console[f]).toHaveBeenCalledWith("[namespace] hello");
    });

    it(`should use a class name as namespace`, () => {
      class FakeClass {}
      const l = logger(FakeClass);
      l[f]("hello");
      expect(window.console[f]).toHaveBeenCalledWith("[FakeClass] hello");
    });

    it(`should use the level as namespace`, () => {
      const l = logger();
      l[f]("hello");

      // @ts-ignore
      const level = Object.entries(LogLevel).find(([, x]) => x === f)[0];

      expect(window.console[f]).toHaveBeenCalledWith(`[${level}] hello`);
    });

    test(`log('${f}')('hello') should call 'console.${f}'`, () => {
      const l = logger("x");
      l.log(f as LogLevel)("hello");

      expect(window.console[f]).toHaveBeenCalledWith(`[x] hello`);
    });
  }
);
