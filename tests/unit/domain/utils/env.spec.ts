import { EnvironmentVariable, env, EnvVars } from "@/domain/utils/env";

describe("EnvironmentVariable", () => {
  describe("is", () => {
    it.each([true, "hello", 1234])(
      "should return true when values are equal `%s`",
      (x) => {
        expect(
          new EnvironmentVariable("enviromentVariable", () => String(x)).is(x)
        ).toBe(true);
      }
    );
  });
  describe("getOrElse", () => {
    it("should return the value, when it's not `undefined`", () => {
      expect(new EnvironmentVariable("x", () => "hello").getOrElse()).toBe(
        "hello"
      );
    });
    it("should return the default value when it's `undefined`", () => {
      expect(
        new EnvironmentVariable("x", () => undefined).getOrElse("defaultValue")
      ).toBe("defaultValue");
    });
    it("should return undefined, when both the value AND the default value are `undefined`", () => {
      expect(
        new EnvironmentVariable("x", () => undefined).getOrElse()
      ).toBeUndefined();
    });
  });
  describe("map", () => {
    it("should map a value", () => {
      expect(
        new EnvironmentVariable("x", () => "2").map((x) => Number(x) + 2)
      ).toBe(4);
    });
    it("should not map when undefined", () => {
      expect(
        new EnvironmentVariable("x", () => undefined).map((x) => x + ",")
      ).toBeUndefined();
    });
  });
  describe("isTrue", () => {
    it("should return true, when the value is 'true'", () => {
      expect(new EnvironmentVariable("x", () => "true").isTrue()).toBe(true);
    });
    it.each(["false", "zorglu", "1234", undefined])(
      "should return false, if the value is anything else than 'true': '%s'",
      (value) => {
        expect(new EnvironmentVariable("x", () => value).isTrue()).toBe(false);
      }
    );
  });
  describe("isFalse", () => {
    it.each(["1234", "zorglu", undefined])(
      "should return true, when value is '%s'",
      (value) => {
        expect(new EnvironmentVariable("x", () => value).isFalse()).toBe(true);
      }
    );

    it("should return `false` when the value is `true`", () => {
      expect(new EnvironmentVariable("x", () => "true").isFalse()).toBe(false);
    });
  });
});

describe("env", () => {
  beforeEach(() => {
    process.env.TEST_VALUE_1 = "1";
    process.env.TEST_VALUE_TRUE = "true";
    process.env.TEST_VALUE_FALSE = "false";
    process.env.TEST_VALUE_FIZZ = "fizz";
    process.env.TEST_VALUE_NOT_DEFINED;
  });

  describe("is", () => {
    it.each([
      ["1", 1],
      ["FIZZ", "fizz"],
      ["TRUE", true],
      ["FALSE", false],
    ])("should return true when value is `%s`", (val, value) => {
      // @ts-ignore
      expect(env(`TEST_VALUE_${val}`).is(value)).toBe(true);
    });
  });

  describe("map", () => {
    it.each([
      ["1", "1", "11"],
      ["FIZZ", "fizz", "fizz1"],
      ["TRUE", true, "true1"],
      ["FALSE", false, "false1"],
      ["NOT_DEFINED", undefined, undefined],
    ])(
      "should map `process.env.TEST_VALUE_%s` from '%s' to '%s'",
      (val, _, expected) => {
        expect(env(`TEST_VALUE_${val}` as EnvVars).map((x) => x + 1)).toBe(
          expected
        );
      }
    );
  });

  describe("getOrElse", () => {
    it.each([
      ["1", "1"],
      ["FIZZ", "fizz"],
      ["TRUE", "true"],
      ["FALSE", "false"],
      ["NOT_DEFINED", undefined],
    ])("should get `process.env.TEST_VALUE_%s` -> %s", (val, expected) => {
      expect(env(`TEST_VALUE_${val}` as EnvVars).getOrElse()).toBe(expected);
    });

    it("should return the default value when `undefined`", () => {
      expect(
        env(`TEST_VALUE_NOT_DEFINED` as EnvVars).getOrElse("default value")
      ).toBe("default value");
    });
  });

  describe("isTrue", () => {
    it("should return true, when `true`", () => {
      expect(env("TEST_VALUE_TRUE" as EnvVars).isTrue()).toBe(true);
    });

    it.each(["1", "FIZZ", "FALSE", "NOT_DEFINED"])(
      "should return `false` when `%s`",
      (val) => {
        expect(env(`TEST_VALUE_${val}` as EnvVars).isTrue()).toBe(false);
      }
    );
  });

  describe("isFalse", () => {
    it("should return false, when `true`", () => {
      expect(env("TEST_VALUE_TRUE" as EnvVars).isFalse()).toBe(false);
    });

    it.each(["1", "FIZZ", "FALSE", "NOT_DEFINED"])(
      "should return `true` when `%s`",
      (val) => {
        expect(env(`TEST_VALUE_${val}` as EnvVars).isFalse()).toBe(true);
      }
    );
  });
});
