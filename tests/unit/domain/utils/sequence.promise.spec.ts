import { sequence } from "@/domain/utils/sequence.promise";

const wait = (ms: number) =>
  new Promise((r) => {
    setTimeout(r, ms);
  });

test("should run promises in sequence", async () => {
  const acc: number[] = [];
  const promises = [
    () =>
      wait(20).then(() => {
        acc.push(1);
      }),
    () =>
      wait(10).then(() => {
        acc.push(2);
      }),
    () => {
      acc.push(3);
    },
  ];

  // @ts-ignore
  await sequence(promises);
  expect(acc).toEqual([1, 2, 3]);
});

test("should ignore failing promises", async () => {
  const acc: number[] = [];
  const promises = [
    () =>
      wait(20).then(() => {
        acc.push(1);
      }),
    () => Promise.reject(new Error("err...")),
    () => {
      acc.push(3);
    },
  ];

  // @ts-ignore
  await sequence(promises);
  expect(acc).toEqual([1, 3]);
});
