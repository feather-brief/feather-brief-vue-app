import { FeedWithItems, asFeedWithItems } from "@/domain/FeedWithItems";
import { FeedItem } from "@/domain/FeedItem";
import {
  FeedItemGenerator,
  FeedWithItemsGenerator,
} from "../utils/FeedWithItemsGenerator";

test("feedWithItemsConversion", () => {
  const feedItems: FeedItem[] = Array.from({ length: 4 })
    .map(() =>
      new FeedItemGenerator("feedName", "https://feedUrl.tld").generate()
    )
    .concat(
      Array.from({ length: 6 }).map(() =>
        new FeedItemGenerator(
          "Another Feed",
          "https://another-feed.io"
        ).generate()
      )
    );

  const feedWithItems: FeedWithItems[] = asFeedWithItems(feedItems);

  // There are only 2 feeds
  expect(feedWithItems).toHaveLength(2);
  // But 10 FeedItems
  expect(feedWithItems.flatMap((f) => f.items)).toHaveLength(10);

  feedWithItems.forEach((feed) => {
    feed.items.forEach((i) => {
      expect(i.feedUrl).toBe(feed.feedUrl);
    });
  });
});

test("duplicate are deleted", () => {
  const fwi = new FeedWithItemsGenerator("feedName", "feedurl")
    .withItems(2)
    .generate();

  fwi.items[1].articleUrl = fwi.items[0].articleUrl;

  const feeds = asFeedWithItems(fwi.items);

  expect(feeds).toHaveLength(1);
  expect(feeds[0].items).toHaveLength(1);
});
