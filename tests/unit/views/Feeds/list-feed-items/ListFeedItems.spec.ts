import { shallowMount } from "@vue/test-utils";
import { Store } from "vuex";
import { createFeatherBriefApp } from "../../../utils/createFeatherBriefApp";
import ListFeedItems from "@/views/Feeds/ListFeedItems.vue";
import { FeatherBriefState } from "@/store/useStore";
import { asFeedWithItems } from "@/domain/FeedWithItems";
import STUB_FEED_ITEMS from "@/assets/stubs/feeditems.json";
import { OFFLINE_ARTICLES } from "@/domain/constants";

const SAVED_FEED_ITEM = {
  ...STUB_FEED_ITEMS[0],
  saved: true,
  shown: false,
  read: false,
};
const REMOTE_FEED_ITEM = {
  ...STUB_FEED_ITEMS[1],
  saved: false,
  shown: false,
  read: false,
};

async function getWrapper(feedName: string) {
  const feedsWithItems = await asFeedWithItems([
    SAVED_FEED_ITEM,
    REMOTE_FEED_ITEM,
  ]);
  const app = await createFeatherBriefApp({ shouldDispatch: false });
  app.store = new Store<FeatherBriefState>({
    state: {
      feedsWithItems,
      synced: true,
      deletedUrls: [],
      loading: false,
      newVersionFound: false,
      userFeeds: [],
    },
  });

  return shallowMount(ListFeedItems, {
    ...app,
    propsData: {
      feedName,
    },
    stubs: ["offline-feeditems-list", "feeditems-list", "delete-feed-button"],
  });
}

test("ListFeedItems with offline articles", async () => {
  expect(await getWrapper(OFFLINE_ARTICLES)).toMatchSnapshot();
});

test("ListFeedItems with remote articles", async () => {
  expect(await getWrapper(REMOTE_FEED_ITEM.feedName)).toMatchSnapshot();
});
