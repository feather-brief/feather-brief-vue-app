import { shallowMount } from "@vue/test-utils";
import STUB_FEED_ITEMS from "@/assets/stubs/feeditems.json";
import ListFeeds from "@/views/Feeds/ListFeeds.vue";
import { createFeatherBriefApp } from "../../../utils/createFeatherBriefApp";
import { Store } from "vuex";
import { FeatherBriefState, getters } from "@/store/useStore";
import { asFeedWithItems } from "@/domain/FeedWithItems";
import { Actions as A } from "@/store/Actions";
import { Getters as G } from "@/store/Getters";
import { FeedItem } from "@/domain/FeedItem";

async function getWrapper(...feedItems: FeedItem[]) {
  document.body.setAttribute("data-app", "true");
  const featherBriefApp = await createFeatherBriefApp({
    shouldDispatch: false,
  });
  const feedsWithItems = await asFeedWithItems(
    feedItems.length > 0
      ? feedItems
      : [{ ...STUB_FEED_ITEMS[0], saved: true, read: false, shown: false }]
  );

  return shallowMount(
    ListFeeds,
    Object.assign(featherBriefApp, {
      store: new Store<FeatherBriefState>({
        state: {
          feedsWithItems,
          loading: false,
          newVersionFound: false,
          synced: true,
          deletedUrls: [],
          userFeeds: [],
        },
        actions: {
          [A.refreshFeedItems]: () => {
            return Promise.resolve();
          },
          [A.refreshUserFeeds]: () => {
            return Promise.resolve();
          },
        },
        getters: {
          [G.notShownNorSavedFeedItems]: getters[G.notShownNorSavedFeedItems],
        },
      }),
    })
  );
}
test("should have offline articles", async () => {
  expect(((await getWrapper()).vm as any).hasOfflineFeeds).toBe(true);
});

test("should show saved articles at the top of the list", async () => {
  expect(await getWrapper()).toMatchSnapshot();
});
