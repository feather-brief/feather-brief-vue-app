const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");

module.exports = {
  transpileDependencies: ["vuetify"],
  publicPath: "/",

  pwa: {
    name: "Telary news",
    themeColor: "#121212",
    iconPaths: {
      favicon16: "img/icons/fb-icon-16.png",
      favicon32: "img/icons/fb-icon-32.png",
      appleTouchIcon: "img/icons/fb-icon-152.png",
    },
    manifestOptions: {
      short_name: "FB",
      icons: [
        {
          src: "/img/icons/fb-icon-192.png",
          sizes: "192x192",
          type: "image/png",
        },
      ],
    },
    workboxOptions: {
      skipWaiting: true,
    },
  },

  css: {
    requireModuleExtension: false,
    loaderOptions: {
      css: {
        modules: {
          auto(path) {
            return (
              path.endsWith(".module.css") &&
              !path.endsWith("codeHighlight.module.css")
            );
          },
        },
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: "fr",
      fallbackLocale: "fr",
      localeDir: "locales",
      enableInSFC: false,
    },
  },

  configureWebpack: {
    plugins: [
      new LodashModuleReplacementPlugin({
        collections: true,
        currying: true,
        flattening: true,
      }),
    ],
  },
};
