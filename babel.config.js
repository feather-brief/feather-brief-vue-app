module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  plugins: [
    [
      "prismjs",
      {
        languages: [
          "bash",
          "c",
          "cpp",
          "csharp",
          "css",
          "docker",
          "elm",
          "git",
          "go",
          "graphql",
          "haskell",
          "http",
          "ini",
          "java",
          "javascript",
          "json",
          "jsx",
          "kotlin",
          "markup",
          "php",
          "python",
          "ruby",
          "rust",
          "sql",
          "tsx",
          "typescript",
          "yaml",
        ],
        plugins: [],
        css: false,
      },
    ],
    "lodash",
  ],
  env: {
    test: {
      presets: [
        [
          "@babel/preset-env",
          {
            targets: { node: "current" },
          },
        ],
      ],
      plugins: ["dynamic-import-node"],
    },
  },
};
