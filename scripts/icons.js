/* eslint-disable */
const { resolve, join } = require("path");
const sharp = require("sharp");
const logger = require("signale");

const ASSET_DIR = resolve(__dirname, "..", "src", "assets");
const PUBLIC_DIR = resolve(__dirname, "..", "public");
const ICON = join(ASSET_DIR, "icon.png");
// const SPLASH = join(ASSET_DIR, "splash.png");

function pngs() {
  return Promise.all(
    [16, 32, 48, 57, 72, 76, 120, 128, 152, 192].map((dim) =>
      sharp(ICON)
        .resize(dim, dim)
        .toFile(join(PUBLIC_DIR, "img", "icons", `fb-icon-${dim}.png`))
    )
  );
}

function favicon() {
  return sharp(join(PUBLIC_DIR, "img", "icons", "fb-icon-32.png")).toFile(
    join(PUBLIC_DIR, "favicon.ico")
  );
}

logger.info("Starting creating icons");
Promise.resolve()
  .then(pngs)
  .then(favicon)
  .then(() => {
    logger.success("DONE!");
  })
  .catch((err) => {
    logger.fatal("ERROR", err.message);
    logger.error(err.stack);
  });
