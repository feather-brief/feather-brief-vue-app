import { CombinedVueInstance, Vue as VueType } from "vue/types/vue";

import {
  Auth0Client,
  RedirectLoginOptions,
  GetIdTokenClaimsOptions,
  GetTokenSilentlyOptions,
  GetTokenWithPopupOptions,
  LogoutOptions,
  IdToken,
  PopupLoginOptions,
} from "@auth0/auth0-spa-js";

export interface Auth0VuePluginProperties {
  loading: boolean;
  isAuthenticated: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  user: Record<string, any> | undefined;
  popupOpen: boolean;
  error: Error | null;
  auth0Client: Auth0Client | null;
}

export interface Auth0VuePluginMethods {
  loginWithPopup(o?: PopupLoginOptions): Promise<void>;
  handleRedirectCallback(): Promise<void>;
  loginWithRedirect(o?: RedirectLoginOptions): Promise<void>;
  getIdTokenClaims(o?: GetIdTokenClaimsOptions): Promise<IdToken>;
  getTokenSilently(o?: GetTokenSilentlyOptions): Promise<string>;
  getTokenWithPopup(o?: GetTokenWithPopupOptions): Promise<string>;
  logout(o?: LogoutOptions): void;
}

export interface Auth0VuePluginComputed {
  maybeAuthClient: Auth0Client;
}

export type Auth0VuePlugin = Auth0VuePluginProperties &
  Auth0VuePluginMethods &
  Auth0VuePluginComputed;

export interface UseAuthParameters {
  onRedirectCallback: (appState: any) => void;
  redirectUri: string;
  [id: string]: any;
}

export type AuthService = CombinedVueInstance<
  VueType,
  Auth0VuePluginProperties,
  Auth0VuePluginMethods,
  Auth0VuePluginComputed,
  object
>;
