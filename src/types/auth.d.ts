import Vue from "vue";
import { CombinedVueInstance } from "vue/types/vue";
import { Framework } from "vuetify";
import type { Flags } from "@/plugins/flags/flags.enum";
import {
  Auth0VuePluginProperties,
  Auth0VuePluginMethods,
} from "./Auth0VuePlugin";
import { CachedFeatherBrief } from "@/plugins/feather-brief/CachedFeatherBrief";

declare module "vue/types/vue" {
  interface Vue {
    $auth: AuthService;

    $vuetify: Framework;

    $fb: CachedFeatherBrief;

    $flags: Record<Flags, boolean>;
  }
}
