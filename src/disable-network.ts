import { setupWorker, rest } from "msw";
import { FeatherBriefRestInterface } from "@/plugins/feather-brief/FeatherBrief";
import feeditems from "@/assets/stubs/feeditems.json";

import { FeedItem } from "./domain/FeedItem";
import { env, EnvVars as E } from "@/domain/utils/env";
import { FeatherBriefArticleParsed } from "./plugins/feather-brief/IFeatherBrief";
import { Feed } from "./domain/Feed";
import { ArticleStatus } from "./domain/ArticleStatus";

let ourFeedItems: (FeedItem & { content: string })[] = feeditems;

function wait(ms: number) {
  return new Promise<void>((r) => {
    setTimeout(r, ms);
  });
}

function getFeeds() {
  return ourFeedItems
    .map((item) => ({ url: item.feedUrl, name: item.feedName }))
    .reduce(
      (feeds, item) =>
        feeds.some((f: { url: string; name: string }) => f.url === item.url)
          ? feeds
          : feeds.concat(item),
      [] as { url: string; name: string }[]
    );
}

export const DEFAULT_BASE_URL = env(E.USE_DOMAIN).isTrue()
  ? "https://cn-ds.fr"
  : "https://example.com";

export const handlers = (BASE_URL = DEFAULT_BASE_URL) => [
  rest.get(
    `${BASE_URL}${FeatherBriefRestInterface.FEED_ITEMS}`,
    (req, res, ctx) => {
      let items = ourFeedItems;

      if (req.url.searchParams.has("feed")) {
        const feedUrl = req.url.searchParams.get("feed");
        items = items.filter((i) => i.feedUrl === feedUrl);
      }

      if (req.url.searchParams.has("filter")) {
        // @ts-ignore
        const filters: ArticleStatus[] = req.url.searchParams.getAll("filter");
        items = items.filter((i) =>
          filters.some(
            (f) => Object.prototype.hasOwnProperty.call(i, f) && i[f]
          )
        );
      }

      return res(
        ctx.json({
          items:
            items.length === ourFeedItems.length
              ? items.filter((i) => !i.shown && !i.saved)
              : items,
        })
      );
    }
  ),
  rest.put(
    `${BASE_URL}${FeatherBriefRestInterface.FEED_ITEMS}`,
    (req, res, ctx) => {
      if (!["markAs", "unmarkAs"].some((x) => req.url.searchParams.has(x))) {
        return res(ctx.status(400));
      }

      async function modifyData() {
        // @ts-ignore
        const urls: string[] = req.body?.articles;

        // ES modifications are not immediate,
        // we simulate this with `wait`
        await wait(300);

        const modification = req.url.searchParams.has("markAs")
          ? { [req.url.searchParams.get("markAs") || "saved"]: true }
          : { [req.url.searchParams.get("unmarkAs") || "saved"]: false };

        ourFeedItems = ourFeedItems.map((f) => {
          return urls.includes(f.articleUrl)
            ? Object.assign(f, modification)
            : f;
        });
      }

      // We don't await for the promise, since this operation
      // should take place in the background.
      modifyData();

      return res(ctx.status(204));
    }
  ),
  rest.get(`${BASE_URL}${FeatherBriefRestInterface.PARSE}`, (req, res, ctx) => {
    if (!req.url.searchParams.has("url")) {
      return res(
        ctx.status(400),
        ctx.json({ status: "KO", message: "Bad format" })
      );
    }

    const url = req.url.searchParams.get("url");

    const article = ourFeedItems.find((a) => a.articleUrl === url);

    if (!article) {
      return res(ctx.status(404), ctx.json({}));
    }

    return res(
      ctx.json({
        title: article.title,
        content: article.content,
        length: article.content.length,
        siteName: article.feedName,
        excerpt:
          article.content.length > 100
            ? article.content.slice(0, 100) + "..."
            : article.content,
        byline: article.author,
      } as FeatherBriefArticleParsed)
    );
  }),
  rest.get(
    `${BASE_URL}${FeatherBriefRestInterface.EXPLORE_FEEDS}`,
    (_, res, ctx) =>
      res(
        ctx.json({
          feeds: ourFeedItems
            .map((item) => ({ url: item.feedUrl, name: item.feedName }))
            .reduce(
              (feeds, item) =>
                feeds.some(
                  (f: { url: string; name: string }) => f.url === item.url
                )
                  ? feeds
                  : feeds.concat(item),
              [] as { url: string; name: string }[]
            ),
        })
      )
  ),
  rest.put(`${BASE_URL}${FeatherBriefRestInterface.OPML}`, (_, res, ctx) =>
    res(ctx.status(200))
  ),
  rest.get(`${BASE_URL}${FeatherBriefRestInterface.FEEDS}`, (_, res, ctx) => {
    return res(
      ctx.json({
        feeds: ourFeedItems
          .map((i) => ({
            feedName: i.feedName,
            feedUrl: i.feedUrl,
          }))
          .reduce(
            (feeds, feed) =>
              feeds.some((f) => f.feedUrl === feed.feedUrl)
                ? feeds
                : feeds.concat(feed),
            [] as Feed[]
          )
          .concat([
            {
              feedUrl: "https://medium.com/feed/airbnb-engineering",
              feedName: "Airbnb Engineering",
            },
            {
              feedUrl: "http://feeds.feedburner.com/GDBcode",
              feedName: "Google Developers Blog",
            },
            {
              feedUrl: "http://feeds.feedburner.com/HighScalability",
              feedName: "High Scalability",
            },
            {
              feedUrl: "https://code.facebook.com/posts/rss/",
              feedName: "Facebook Engineering",
            },
            {
              feedUrl: "https://netflixtechblog.com/feed",
              feedName: "Netflix Engineering",
            },
          ]),
      })
    );
  }),
  rest.delete(
    `${BASE_URL}${FeatherBriefRestInterface.FEEDS}`,
    (_, res, ctx) => {
      return res(ctx.status(200));
    }
  ),
  rest.get(`${BASE_URL}${FeatherBriefRestInterface.BUNDLES}`, (_, res, ctx) => {
    return res(
      ctx.json([
        {
          name: "Basic Bundle",
          language: "en",
          feeds: ourFeedItems.reduce((feeds, feed) => {
            return feeds.some((f) => feed.feedUrl === f.feedUrl)
              ? feeds
              : feeds.concat({
                  feedName: feed.feedName,
                  feedUrl: feed.feedUrl,
                });
          }, [] as Feed[]),
        },
      ])
    );
  }),
  rest.post(`${BASE_URL}${FeatherBriefRestInterface.FEEDS}`, (_, res, ctx) =>
    res(ctx.status(200))
  ),
];

export const worker = () => setupWorker(...handlers());
