export enum Getters {
  notShownNorSavedFeedItems = "notShownNorSavedFeedItems",
  userFeedsNotInReadingList = "userFeedsNotInReadingList",
  byUrl = "byUrl",
  itemsGroupedByCategory = "itemsGroupedByCategory",
  allCategories = "allCategories",
}
