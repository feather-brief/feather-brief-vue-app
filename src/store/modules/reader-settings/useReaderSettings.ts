import { ActionTree, GetterTree, Module, MutationTree } from "vuex";
import { FontFamilyVars } from "@/plugins/font-family/FontFamily.plugin";
import { FeatherBriefState } from "@/store/useStore";
import { Mutations } from "@/store/modules/reader-settings/Mutations";
import { Actions } from "@/store/modules/reader-settings/Actions";
import { Getters } from "@/store/modules/reader-settings/Getters";

export const READER_SETTINGS_MODULE = "readerSettings";

export interface ReaderSettingsStyle {
  theme: "dark" | "light";
  lineHeight: number;
  textAlign: "justify" | "left" | "right" | "center";
  fontFamily: FontFamilyVars.Serif | FontFamilyVars.Sans;
  fontSize: number;
  fontWeight: 100 | 200 | 300 | 400 | 500 | 600 | 700 | 900;
  maxWidth: number;
}

export interface ReaderSettingsState {
  style: ReaderSettingsStyle;
}

const defaultStyle: ReaderSettingsStyle = {
  theme: "dark",
  lineHeight: 1.5,
  textAlign: "justify",
  fontSize: 16,
  fontFamily: FontFamilyVars.Sans,
  fontWeight: 500,
  maxWidth: 770,
};

const initialState: ReaderSettingsState = {
  style: { ...defaultStyle },
};

export const getters: GetterTree<ReaderSettingsState, FeatherBriefState> = {
  [Getters.STYLE]: (state) => state.style,
};

const mutations: MutationTree<ReaderSettingsState> = {
  [Mutations.setStyle]: (state, style: Partial<ReaderSettingsStyle>) => {
    Object.assign(state.style, style);
  },
};

const actions: ActionTree<ReaderSettingsState, FeatherBriefState> = {
  [Actions.setStyle]: ({ commit }, style: Partial<ReaderSettingsStyle>) => {
    commit(Mutations.setStyle, style);
  },
  [Actions.reinit]: ({ commit }) => {
    commit(Mutations.setStyle, { ...defaultStyle });
  },
};

export function useReaderSettingsStore(): Module<
  ReaderSettingsState,
  FeatherBriefState
> {
  return {
    namespaced: false,
    state: initialState,
    actions,
    mutations,
    getters,
  };
}
