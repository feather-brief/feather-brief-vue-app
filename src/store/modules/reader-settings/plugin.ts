import { FeatherBriefState } from "@/store/useStore";
import { Plugin, Store } from "vuex";
import { Mutations } from "./Mutations";
import { ReaderSettingsStyle } from "./useReaderSettings";

const PERSISTENCE_KEY = "__TN_READER_STYLE__";

export function isConfiguredThemeDark(): boolean {
  return (
    (JSON.parse(
      window.localStorage.getItem(PERSISTENCE_KEY) ?? '{"theme":"dark"}'
    ) as undefined | ReaderSettingsStyle)?.theme !== "light"
  );
}

export function createReaderSettingsPlugin(): Plugin<FeatherBriefState> {
  return (store: Store<FeatherBriefState>) => {
    const fromLocalStorage = window.localStorage.getItem(PERSISTENCE_KEY);
    if (fromLocalStorage) {
      store.commit(Mutations.setStyle, JSON.parse(fromLocalStorage));
    }

    store.subscribe((mutation) => {
      if (mutation.type === Mutations.setStyle) {
        window.localStorage.setItem(
          PERSISTENCE_KEY,
          JSON.stringify(mutation.payload)
        );
      }
    });
  };
}
