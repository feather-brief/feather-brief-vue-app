import { Module } from "vuex";
import { Getters } from "./Getters";
import { Mutations } from "./Mutations";
import { Actions } from "./Actions";
import { patternFromWord } from "@/domain/utils/gradientFromWord";
import { FeatherBriefState } from "@/store/useStore";

export interface PatternHistory {
  color?: string;
  background: string;
}

export interface PatternsState {
  patterns: Record<string, PatternHistory>;
}

type S = PatternsState;

export const PATTERNS_MODULE = "patterns";

interface QueryMutationAddPattern {
  key: string;
  pattern: PatternHistory;
}

export interface QueryActionAddPattern {
  key: string;
  pattern: PatternHistory;
}

export interface QueryActionAddKey {
  key: string;
}

export function usePatternsStore(): Module<S, FeatherBriefState> {
  return {
    namespaced: false,
    state(): S {
      return {
        patterns: {},
      };
    },
    getters: {
      [Getters.byName]: (state: S) => (
        name: string
      ): PatternHistory | undefined => {
        return state.patterns[name];
      },
    },
    mutations: {
      [Mutations.addPattern]: (state: S, query: QueryMutationAddPattern) => {
        state.patterns = {
          ...state.patterns,
          [query.key]: query.pattern,
        };
      },
    },
    actions: {
      [Actions.addPattern]: ({ commit }, query: QueryActionAddPattern) => {
        const mutationQuery: QueryMutationAddPattern = {
          key: query.key,
          pattern: query.pattern,
        };
        commit(Mutations.addPattern, mutationQuery);
      },
      [Actions.addKey]: async (
        { commit, getters },
        query: QueryActionAddKey
      ) => {
        const got = getters[Getters.byName](query.key);
        if (got) {
          return;
        }

        const pattern = await patternFromWord(query.key);
        const mutationQuery: QueryMutationAddPattern = {
          key: query.key,
          pattern: {
            color: pattern.background?.color.hex().toString(),
            background: pattern.toDataURL(),
          },
        };
        commit(Mutations.addPattern, mutationQuery);
      },
    },
  };
}
