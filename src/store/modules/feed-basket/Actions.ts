export enum FeedBasketActions {
  addFeed = "feedbasket.addFeed",
  removeFeed = "feedbasket.removeFeed",
}
