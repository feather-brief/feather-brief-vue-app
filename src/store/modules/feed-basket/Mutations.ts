export enum FeedBasketMutations {
  add = "feedbasket.add",
  remove = "feedbasket.remove",
  empty = "feedbasket.empty",
}
