import { Module } from "vuex";
import { Feed } from "@/domain/Feed";
import { FeatherBriefState } from "@/store/useStore";
import { FeedBasketActions as A } from "@/store/modules/feed-basket/Actions";
import { FeedBasketMutations as M } from "@/store/modules/feed-basket/Mutations";
import { FeedBasketGetters as G } from "@/store/modules/feed-basket/Getters";

export interface FeedBasketState {
  basket: Feed[];
}

export const FEED_BASKET_MODULE = "feedbasket";

export function useFeedBasketStore(): Module<
  FeedBasketState,
  FeatherBriefState
> {
  return {
    state: {
      basket: [],
    },
    getters: {
      [G.exists]: (state) => (feed: Feed) => {
        return state.basket.some((f) => f.feedUrl === feed.feedUrl);
      },
      [G.isEmpty]: (state) => {
        return state.basket.length === 0;
      },
    },
    mutations: {
      [M.add]: (state, feed: Feed) => {
        state.basket.push(feed);
      },
      [M.remove]: (state, feed: Feed) => {
        state.basket = state.basket.filter((f) => f.feedUrl !== feed.feedUrl);
      },
      [M.empty]: (state) => {
        state.basket = [];
      },
    },
    actions: {
      [A.addFeed]: ({ commit }, feed: Feed) => {
        commit(M.add, feed);
      },
      [A.removeFeed]: ({ commit }, feed: Feed) => {
        commit(M.remove, feed);
      },
    },
  };
}
