export enum Mutations {
  updateNewVersionFound = "updateNewVersionFound",
  updateFeedWithItems = "updateFeedWithItems",
  updateUserFeeds = "updateUserFeeds",
  updateSynced = "updateSynced",
  updateStatus = "updateStatus",
  updateDeleted = "updateDeleted",
  updateFeedCategory = "updateFeedCategory",
}
