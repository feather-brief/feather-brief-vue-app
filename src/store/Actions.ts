export enum Actions {
  didFindNewVersion = "didFindNewVersion",
  refreshFeedItems = "refreshFeedItems",
  refreshUserFeeds = "refreshUserFeeds",
  deleteFeedItems = "deleteFeedItems",
  deleteFeed = "deleteFeed",
  markFeedItems = "markFeedItems",
  unmarkFeedItems = "unmarkFeedItems",
  offloadArticle = "offloadArticle",
  loading = "loading",
  doneLoading = "doneLoading",
  updateFeedCategory = "updateFeedCategory",
}
