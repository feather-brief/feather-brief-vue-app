import { ArticleStatus } from "@/domain/ArticleStatus";
import { Feed } from "@/domain/Feed";
import { DEFAULT_CATEGORY, FeedItem } from "@/domain/FeedItem";
import { asFeedWithItems, FeedWithItems } from "@/domain/FeedWithItems";
import { CachedFeatherBrief } from "@/plugins/feather-brief/CachedFeatherBrief";
import { useFeatherBrief } from "@/plugins/feather-brief/FeatherBriefPlugin";
import { isNil } from "lodash";
import {
  filter,
  flatMap,
  flow,
  getOr,
  map,
  uniq,
  groupBy,
  mapValues,
} from "lodash/fp";
import Vue from "vue";
import Vuex, { ActionTree, GetterTree, MutationTree } from "vuex";
import { Actions as A } from "./Actions";
import { Getters } from "./Getters";
import { Mutations as M } from "./Mutations";
import {
  FEED_BASKET_MODULE,
  useFeedBasketStore,
} from "./modules/feed-basket/useFeedBasketStore";
import { PATTERNS_MODULE, usePatternsStore } from "./modules/patterns";
import {
  createReaderSettingsPlugin,
  READER_SETTINGS_MODULE,
  useReaderSettingsStore,
} from "@/store/modules/reader-settings";

export interface FeatherBriefState {
  synced: boolean;
  feedsWithItems: FeedWithItems[];
  userFeeds: Feed[];
  deletedUrls: string[];
  newVersionFound: boolean;
  loading: boolean;
}

export interface MarkUnmarkFeedItemPayload {
  urls: string[];
  status: ArticleStatus;
}

export function actions(
  $fb: CachedFeatherBrief
): ActionTree<FeatherBriefState, FeatherBriefState> {
  return {
    [A.didFindNewVersion]: ({ commit }) => {
      commit(M.updateNewVersionFound, true);
    },
    [A.refreshFeedItems]: async ({ commit, state }) => {
      await $fb.invalidate();
      const feedWithItems = await $fb.getLastFeedItems();
      commit(M.updateFeedWithItems, feedWithItems);

      if (!state.synced) {
        commit(M.updateSynced, true);
      }
    },
    [A.deleteFeedItems]: async ({ commit, state }, urls: string | string[]) => {
      if (typeof urls === "string") {
        urls = [urls];
      }

      await $fb.mark(urls, ArticleStatus.SHOWN);

      commit(M.updateDeleted, urls);
    },
    [A.deleteFeed]: ({ commit, state }, feedUrl: string) => {
      commit(
        M.updateFeedWithItems,
        state.feedsWithItems.filter((feed) => feed.feedUrl !== feedUrl)
      );
    },
    [A.markFeedItems]: async (
      { commit },
      { urls, status }: MarkUnmarkFeedItemPayload
    ) => {
      await $fb.mark(urls, status);
      commit(M.updateStatus, {
        urls,
        status,
        value: true,
      });
    },
    [A.unmarkFeedItems]: async (
      { commit },
      { urls, status }: MarkUnmarkFeedItemPayload
    ) => {
      await $fb.unmark(urls, status);
      commit(M.updateStatus, {
        urls,
        status,
        value: false,
      });
    },
    [A.offloadArticle]: async ({ commit }, feedItem: FeedItem) => {
      await $fb.offloadArticle(feedItem);
      commit(M.updateStatus, {
        urls: [feedItem.articleUrl],
        status: ArticleStatus.SAVED,
        value: false,
      });
    },
    [A.loading]: ({ state }) => {
      state.loading = true;
    },
    [A.doneLoading]: ({ state }) => {
      state.loading = false;
    },
    [A.refreshUserFeeds]: async ({ dispatch, commit }) => {
      await dispatch(A.loading);
      const feeds = await $fb.getUserFeeds();
      commit(M.updateUserFeeds, feeds);
      await dispatch(A.doneLoading);
    },
    [A.updateFeedCategory]: async (
      { dispatch, commit },
      { feed, category }: { feed: Feed; category: string }
    ) => {
      const updatedFeed = await $fb.updateFeedCategory(feed, category);
      commit(M.updateFeedCategory, updatedFeed);
      dispatch(A.refreshUserFeeds);
    },
  };
}

export const mutations: MutationTree<FeatherBriefState> = {
  [M.updateNewVersionFound]: (state, newValue: boolean) => {
    state.newVersionFound = newValue;
  },
  [M.updateFeedWithItems]: (state, feedWithItems: FeedWithItems[]) => {
    state.feedsWithItems = feedWithItems.map((feed) => {
      feed.items = feed.items.filter(
        (item) => !state.deletedUrls.includes(item.articleUrl)
      );
      return feed;
    });
  },
  [M.updateSynced]: (state, synced: boolean) => {
    state.synced = synced;
  },
  [M.updateStatus]: (
    state,
    {
      urls,
      status,
      value,
    }: { urls: string[]; status: ArticleStatus; value: boolean }
  ) => {
    state.feedsWithItems = state.feedsWithItems.map((feed) => {
      feed.items = feed.items.map((item) => {
        if (!urls.includes(item.articleUrl)) {
          return item;
        }

        const newState = {
          ...item,
          [status]: value,
        };

        // We can only upvote OR downvote, but not both
        if (status === ArticleStatus.DOWNVOTE && value) {
          newState[ArticleStatus.UPVOTE] = false;
        } else if (status === ArticleStatus.UPVOTE && value) {
          newState[ArticleStatus.DOWNVOTE] = false;
        }

        return newState;
      });

      return feed;
    });
  },
  [M.updateDeleted]: (state, urls: string[]) => {
    state.deletedUrls = state.deletedUrls.concat(Array.from(new Set(urls)));
    state.feedsWithItems = state.feedsWithItems
      .map((feed) => {
        feed.items = feed.items.filter((f) => !urls.includes(f.articleUrl));

        if (feed.items.length === 0) {
          return undefined;
        }

        return feed;
      })
      .filter(Boolean) as FeedWithItems[];
  },
  [M.updateUserFeeds]: (state, feeds: Feed[]) => {
    state.userFeeds = feeds;

    state.feedsWithItems.forEach((feedWithItems: FeedWithItems, feedIndex) => {
      const feed = feeds.find((feed) => feed.feedUrl === feedWithItems.feedUrl);
      if (isNil(feed)) {
        return;
      }

      Vue.set(
        state.feedsWithItems,
        feedIndex,
        Object.assign(feedWithItems, { category: feed.category })
      );

      feedWithItems.items.forEach((feedItem: FeedItem, feedItemIndex) => {
        Vue.set(
          state.feedsWithItems[feedIndex].items,
          feedItemIndex,
          Object.assign(feedItem, { category: feed.category })
        );
      });
    });
  },
  [M.updateFeedCategory]: (state, feed: Feed) => {
    let idx = state.feedsWithItems.findIndex(
      (feed) => feed.feedUrl === feed.feedUrl
    );
    if (idx !== -1) {
      Vue.set(
        state.feedsWithItems,
        idx,
        Object.assign(state.feedsWithItems[idx], { category: feed.category })
      );

      state.feedsWithItems[idx].items.forEach((feedItem, feedItemIndex) => {
        Vue.set(
          state.feedsWithItems[idx].items,
          feedItemIndex,
          Object.assign(feedItem, { category: feed.category })
        );
      });
    }

    idx = state.userFeeds.findIndex((feed) => feed.feedUrl === feed.feedUrl);
    if (idx !== -1) {
      Vue.set(
        state.userFeeds,
        idx,
        Object.assign(state.userFeeds[idx], { category: feed.category })
      );
    }
  },
};

export const getters: GetterTree<FeatherBriefState, FeatherBriefState> = {
  [Getters.notShownNorSavedFeedItems]: (state: FeatherBriefState) => {
    return state.feedsWithItems
      .map((f) => {
        const items = f.items.filter((i) => !i.shown && !i.saved);
        return items.length > 0 ? { ...f, items } : undefined;
      })
      .filter(Boolean);
  },
  [Getters.userFeedsNotInReadingList]: (state: FeatherBriefState) => {
    const feedsInReadingList = new Set(
      state.feedsWithItems.map((f) => f.feedUrl)
    );

    return state.userFeeds.filter(
      (userFeed) => !feedsInReadingList.has(userFeed.feedUrl)
    );
  },
  [Getters.byUrl]: (state: FeatherBriefState) => (
    url: string
  ): FeedItem | undefined => {
    return (
      state.feedsWithItems
        .flatMap((x) => x.items)
        .find((x) => x.articleUrl === url) ??
      ({
        articleUrl: url,
        parsable: true,
      } as FeedItem)
    );
  },
  [Getters.itemsGroupedByCategory]: (state, getters) =>
    flow([
      getOr([] as FeedWithItems[], [
        "feedsWithItems" as keyof FeatherBriefState,
      ]),
      flatMap((x: FeedWithItems) => x.items ?? []),
      filter((i: FeedItem) => !i.shown && !i.saved),
      map((i: FeedItem) => {
        const feedCategory =
          (state.userFeeds ?? []).find((item) => item.feedUrl === i.feedUrl)
            ?.category ??
          i.category ??
          DEFAULT_CATEGORY;
        return { ...i, category: feedCategory };
      }),
      groupBy((i: FeedItem) => i.category ?? DEFAULT_CATEGORY),
      mapValues(asFeedWithItems),
    ])(state),
  [Getters.allCategories]: flow([
    getOr([], ["userFeeds" as keyof FeatherBriefState]),
    map((x: Feed) => x.category),
    uniq,
  ]),
};

export async function useStore(fb?: CachedFeatherBrief) {
  const $fb = fb ?? /* istanbul ignore next */ (await useFeatherBrief());

  return new Vuex.Store<FeatherBriefState>({
    modules: {
      [FEED_BASKET_MODULE]: useFeedBasketStore(),
      [PATTERNS_MODULE]: usePatternsStore(),
      [READER_SETTINGS_MODULE]: useReaderSettingsStore(),
    },
    state: {
      /**
       * `synced` is just a indicator that we synced the
       * store with the remote data.
       * We don't need to be authenticated to use the
       * store, but the majority of the data, requires
       * the authentication.
       */
      synced: false,
      feedsWithItems: [],
      newVersionFound: false,
      loading: false,
      deletedUrls: [],
      userFeeds: [],
    },
    actions: actions($fb),
    mutations,
    getters,
    plugins: [createReaderSettingsPlugin()],
  });
}
