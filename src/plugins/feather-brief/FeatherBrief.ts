import { flow, groupBy, last, mapValues, maxBy, values } from "lodash/fp";
import { IdToken } from "@auth0/auth0-spa-js";
import { ArticleStatus } from "@/domain/ArticleStatus";
import { Bundle } from "@/domain/Bundle";
import { Feed } from "@/domain/Feed";
import { FeedItem } from "@/domain/FeedItem";
import { asFeedWithItems, FeedWithItems } from "@/domain/FeedWithItems";
import { AuthService } from "@/types/Auth0VuePlugin";
import { HttpService } from "./HttpService";
import {
  FeatherBriefArticleFailedParsing,
  FeatherBriefArticleParsed,
  IFeatherBrief,
} from "./IFeatherBrief";

class UnauthenticatedUser extends Error {
  constructor() {
    super("The user doesn't seem connected");
  }
}

interface GetFeedItemsOptions {
  /**
   * The feed URL, to retrieve all items from this feed
   */
  feed?: string;
  /**
   * The number of items to retrieve.
   * If Infinity is provided, this value is removed.
   */
  size?: number;
  /**
   * If all the articles retrieve should have on the
   * status to true.
   */
  filter?: ArticleStatus;
}

/**
 * This class interacts directly with the remote REST interface.
 * Its role is to handle the low level interactions, and errors.
 */
export class FeatherBriefRestInterface {
  constructor(private $auth: AuthService, private $http: HttpService) {}

  private handleUnauthenticated = (defaultValue?: any) => (e?: Error) => {
    if (e instanceof UnauthenticatedUser) {
      return defaultValue;
    }

    throw e ?? /* istanbul ignore next */ new Error();
  };

  private async withToken() {
    const token: IdToken | undefined = await this.$auth.getIdTokenClaims();

    if (!token) {
      throw new UnauthenticatedUser();
    }

    return this.$http.with(token.__raw);
  }

  static FEEDS = "/secured/feeds";
  static FEED_ITEMS = "/secured/feeditems";
  static OPML = "/secured/opml";
  static EXPLORE_FEEDS = "/secured/explore/feeds";
  static BUNDLES = "/secured/bundles";
  static PARSE = "/parse";

  async getUserFeeds<T>(): Promise<T[]> {
    return this.withToken()
      .then((x) => x.GET(FeatherBriefRestInterface.FEEDS))
      .then((x) => {
        if (!x.ok) {
          throw new Error();
        }

        return x.json().catch(() => ({ feeds: [] }));
      })
      .then((x) => {
        if (!("feeds" in x)) {
          throw new Error();
        }

        return x.feeds;
      })
      .then((x) => {
        if (!Array.isArray(x)) {
          throw new Error();
        }

        return x;
      })
      .catch(this.handleUnauthenticated([]))
      .catch(() => {
        throw new Error("There was an error retrieving User Feeds");
      });
  }

  async getFeedItems<T>({
    size = 25,
    ...options
  }: GetFeedItemsOptions = {}): Promise<T[]> {
    return this.withToken()
      .then((x) => {
        const search = Object.entries({
          ...options,
          size: size === Infinity || size <= 0 ? undefined : size,
        }).filter(
          // We remove any undefined value from options
          // since it's stringified as `undefined`
          ([_, v]) => v != undefined
        );

        return x.GET(
          `${FeatherBriefRestInterface.FEED_ITEMS}?${
            // @ts-ignore
            new URLSearchParams(search).toString()
          }`
        );
      })
      .then((x) => {
        if (!x.ok) {
          throw new Error(`received status ${x.status} from server}`);
        }

        return x.json().catch(() => ({ items: [] }));
      })
      .then((x) => {
        if ("error" in x) {
          throw new Error(`received error from server: ${x.error}`);
        }

        if (!("items" in x)) {
          throw new Error(`the key 'items' does not exist in response body`);
        }

        return x.items;
      })
      .then((x) => {
        if (!Array.isArray(x)) {
          throw new Error(`response body 'items' is not an array`);
        }

        return x;
      })
      .catch(this.handleUnauthenticated([]))
      .catch((e) => {
        throw new Error(`There was an error retrieving last feed items ${e}`);
      });
  }

  async createFeed(feed: Feed) {
    const res = await this.withToken().then((x) =>
      x.POST(FeatherBriefRestInterface.FEEDS, {
        email: this.$auth.user?.email,
        name: feed.feedName,
        url: feed.feedUrl,
      })
    );

    switch (res.status) {
      case 304:
        throw new Error("DuplicateFeedError");
      case 500:
        throw new Error("CantProcessFeedError");
      default:
        return feed;
    }
  }

  async updateFeedCategory(feed: Feed, newCategory: string): Promise<Feed> {
    const res = await this.withToken().then((x) =>
      x.PUT(
        FeatherBriefRestInterface.FEEDS +
          "?" +
          new URLSearchParams({
            old_category: feed.category ?? "",
            new_category: newCategory ?? "",
            url: feed.feedUrl ?? "",
            name: feed.feedName ?? "",
          }).toString()
      )
    );

    switch (res.status) {
      case 200:
        return Object.assign(feed, { category: newCategory });
      default:
        throw new Error("CannotUpdateFeedCategory");
    }
  }

  async updateFeedName(feed: Feed, newName: string): Promise<Feed> {
    const res = await this.withToken().then((x) =>
      x.PUT(
        FeatherBriefRestInterface.FEEDS +
          "?" +
          new URLSearchParams({
            old_category: feed.category ?? "",
            new_category: feed.category ?? "",
            url: feed.feedUrl ?? "",
            name: newName ?? feed.feedName ?? "",
          }).toString()
      )
    );

    switch (res.status) {
      case 200:
        return Object.assign(feed, {
          feedName: newName ?? feed.feedName ?? "",
        });
      default:
        throw new Error("CannotUpdateFeedName");
    }
  }

  async parseUrl(url: string): Promise<FeatherBriefArticleParsed> {
    return this.$http
      .GET(`${FeatherBriefRestInterface.PARSE}?url=${encodeURIComponent(url)}`)
      .then((res) => {
        if (!res.ok) {
          throw new Error();
        }

        return res.json();
      })
      .then(
        (
          article: FeatherBriefArticleFailedParsing | FeatherBriefArticleParsed
        ) => {
          if (!("content" in article)) {
            throw new Error("CantParseArticle");
          }

          return article;
        }
      )
      .then(async (article: FeatherBriefArticleParsed) => {
        const $div = document.createElement("div");
        $div.innerHTML = article.content;

        const baseURL = new URL(url).href;

        function transformCode(): Promise<void> {
          const SELECTOR = "pre > code[class*=lang]";
          if (!$div.querySelector(SELECTOR)) {
            return Promise.resolve();
          }

          /* istanbul ignore next */
          return Promise.all([
            import(
              // @ts-ignore
              "@/components/feed-article/codeHighlight.module.css"
            ),
            // NOTE:
            //   The handling of the highlighting, takes place inside
            //   FeedArticle.vue
            import("@/components/feed-article/codeHighlighter"),
          ]).then(() => {});
        }

        async function transformLinks() {
          for (const $node of $div.querySelectorAll("a")) {
            const href = $node.getAttribute("href");

            if (href && !href.startsWith("http")) {
              $node.setAttribute("href", new URL(href, baseURL).href);
            }
          }
        }

        async function transformImg() {
          for (const $node of $div.querySelectorAll("img")) {
            for (const attribute of ["width", "height"]) {
              if ($node.hasAttribute(attribute)) {
                $node.removeAttribute(attribute);
              }
            }

            const src = $node.getAttribute("src");
            if (src && !src.startsWith("http")) {
              $node.setAttribute("src", new URL(src, baseURL).href);
            }

            const srcset = $node.getAttribute("srcset");
            if (
              srcset &&
              srcset.split(",").some((x) => !x.startsWith("http"))
            ) {
              $node.setAttribute(
                "srcset",
                srcset
                  .split(",")
                  .map((srcAndDimension) => {
                    const [src = "", dimension = ""] = srcAndDimension.split(
                      " "
                    );
                    return `${
                      src.startsWith("http") ? src : new URL(src, baseURL).href
                    } ${dimension}`.trim();
                  })
                  .join(",")
              );
            }
          }
        }

        return Promise.all([
          transformCode(),
          transformImg(),
          transformLinks(),
        ]).then(() => {
          return {
            ...article,
            content: $div.innerHTML,
          };
        });
      });
  }

  async updateFeedItems(
    articleUrls: string | string[],
    status: ArticleStatus,
    action: "mark" | "unmark"
  ) {
    const urls = Array.isArray(articleUrls) ? articleUrls : [articleUrls];

    return this.withToken().then((x) =>
      x.PUT(`${FeatherBriefRestInterface.FEED_ITEMS}?${action}As=${status}`, {
        articles: urls,
      })
    );
  }

  /* istanbul ignore next */
  async downloadArticle(feedItem: FeedItem) {
    return;
  }

  /* istanbul ignore next */
  async offloadArticle(feedItem: FeedItem) {
    return;
  }

  async exploreFeeds() {
    return this.withToken()
      .then((x) => x.GET(FeatherBriefRestInterface.EXPLORE_FEEDS))
      .then((x) => x.json())
      .then((x) => {
        if (!("feeds" in x)) {
          throw new Error("Could not explore feeds correctly");
        }

        return x.feeds;
      })
      .then((x: unknown) => {
        if (!Array.isArray(x)) {
          throw new Error("Could not explore feeds correctly");
        }

        return x;
      })
      .then((x: { name: string; url: string }[]) =>
        x.map(({ name, url }) => ({
          feedName: name,
          feedUrl: url,
        }))
      )
      .catch(this.handleUnauthenticated([]));
  }

  uploadOpml(file: File) {
    const fd = new FormData();
    fd.append("opml", file);
    return this.withToken()
      .then((x) => x.PUT(FeatherBriefRestInterface.OPML, fd))
      .then((res) => {
        if (!res.ok) {
          throw new Error();
        }
      });
  }

  deleteFeed(feedUrl: string): Promise<void> {
    return this.withToken()
      .then((x) => x.DELETE(FeatherBriefRestInterface.FEEDS, { url: feedUrl }))
      .then((res) => {
        if (!res.ok) {
          throw new Error();
        }

        return;
      })
      .catch(this.handleUnauthenticated());
  }

  getAllBundles(): Promise<Bundle[]> {
    return this.withToken()
      .then((x) => x.GET(FeatherBriefRestInterface.BUNDLES))
      .then((res) => {
        if (!res.ok) {
          throw new Error();
        }

        return res.json();
      })
      .then((x: Bundle[]) => x)
      .catch(this.handleUnauthenticated([]));
  }
}

/**
 * This is mainly the domain conversion layer.
 * It uses the JSON data from FeatherBriefRestInterface,
 * to transform it to Domain Objects.
 */
export class FeatherBrief implements IFeatherBrief {
  private rest: FeatherBriefRestInterface;

  constructor($auth: AuthService, $http: HttpService) {
    this.rest = new FeatherBriefRestInterface($auth, $http);
  }

  /**
   * Returns all the feeds from the authenticated user.
   */
  async getUserFeeds(): Promise<Feed[]> {
    return this.rest.getUserFeeds<Feed>();
  }

  getLastFeedItems(): Promise<FeedWithItems[]> {
    return this.rest
      .getFeedItems<FeedItem>()
      .then(
        flow([
          groupBy((x: FeedItem) => x.articleUrl),
          mapValues(
            (items: FeedItem[]) =>
              maxBy((x: FeedItem) => x.date, items) ?? last(items)
          ),
          values,
          asFeedWithItems,
        ])
      );
  }

  async getFeedItems(feed: Feed | string): Promise<FeedWithItems | undefined> {
    const feedUrl = typeof feed === "string" ? feed : feed?.feedUrl;

    return asFeedWithItems(
      await this.rest.getFeedItems({
        feed: feedUrl,
        size: Infinity,
      })
    )?.[0];
  }

  async createFeed(feed: Feed): Promise<Feed> {
    if (!feed.feedUrl) {
      throw new Error("Required the feedUrl");
    }

    return this.rest.createFeed(feed);
  }

  async parseUrl(url: string) {
    let thatUrl: URL;
    try {
      thatUrl = new URL(url);
    } catch {
      throw new Error(`the provided url is not a valid URL: '${url}'`);
    }

    return this.rest.parseUrl(thatUrl.toString());
  }

  async mark(article: string | string[], status: ArticleStatus) {
    await this.rest.updateFeedItems(article, status, "mark");
  }

  async unmark(articles: string | string[], status: ArticleStatus) {
    await this.rest.updateFeedItems(articles, status, "unmark");
  }

  async downloadArticle(feedItem: FeedItem): Promise<void> {
    throw new Error("Should not be called");
  }

  async offloadArticle(feeditem: FeedItem): Promise<void> {
    throw new Error("should not be called");
  }

  exploreFeeds(): Promise<Feed[]> {
    return this.rest.exploreFeeds().then((x: unknown) => {
      return x as Feed[];
    });
  }

  uploadOpml(file: File): Promise<void> {
    return this.rest.uploadOpml(file);
  }

  async getSavedFeedItems({ size = 100 } = {}) {
    return this.rest.getFeedItems<FeedItem>({
      size,
      filter: ArticleStatus.SAVED,
    });
  }

  unfollowFeed(feedUrl: string) {
    return this.rest.deleteFeed(feedUrl);
  }

  async getAllBundles(): Promise<Bundle[]> {
    return this.rest.getAllBundles();
  }

  updateFeedCategory(feed: Feed, category: string): Promise<Feed> {
    return this.rest.updateFeedCategory(feed, category);
  }

  updateFeedName(feed: Feed, name: string): Promise<Feed> {
    return this.rest.updateFeedName(feed, name);
  }
}
