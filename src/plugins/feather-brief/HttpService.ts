interface HttpWrapper {
  GET<T>(url: string, body?: FormData | Record<string, T>): Promise<Response>;
  POST<T>(url: string, body?: FormData | Record<string, T>): Promise<Response>;
  PUT<T>(url: string, body?: FormData | Record<string, T>): Promise<Response>;
  PATCH<T>(url: string, body?: FormData | Record<string, T>): Promise<Response>;
  DELETE<T>(
    url: string,
    body?: FormData | Record<string, T>
  ): Promise<Response>;
}

const HTTP_METHODS = ["GET", "POST", "PUT", "PATCH", "DELETE"] as const;
type HttpMethod = typeof HTTP_METHODS[number];

export class HttpService implements HttpWrapper {
  GET = this.request("GET");
  POST = this.request("POST");
  PUT = this.request("PUT");
  PATCH = this.request("PATCH");
  DELETE = this.request("DELETE");

  constructor(private baseURL: string, private token?: string) {}

  private request(method: HttpMethod) {
    return <T>(
      url: string,
      body?: FormData | Record<string, T>
    ): Promise<Response> => {
      const builtURL = this.baseURL.startsWith("http")
        ? new URL(url, this.baseURL)
        : ("/" + this.baseURL + "/" + url).replace(/\/{2,}/g, "/");

      const headers = this.headers;
      if (body && !(body instanceof FormData)) {
        // When the body is a FormData, we don't need
        // to specify the Content-Type.
        // We consider, that in any other case, it _is_ JSON.
        headers.append("Content-Type", "application/json");
      }

      const req = new Request(builtURL.toString(), {
        method,
        headers,
        body: body
          ? body instanceof FormData
            ? body
            : JSON.stringify(body)
          : undefined,
      });

      return fetch(req);
    };
  }

  with(token: string): this {
    this.token = token;
    return this;
  }

  get headers() {
    const h = new Headers();
    h.append("Accept", "application/json");

    if (this.token) {
      h.append("Authorization", `Bearer ${this.token}`);
    }

    return h;
  }
}
