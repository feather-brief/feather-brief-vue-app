import { IFeatherBrief, FeatherBriefArticleParsed } from "./IFeatherBrief";
import { Feed } from "@/domain/Feed";
import { FeedWithItems, asFeedWithItems } from "@/domain/FeedWithItems";
import { ArticleStatus } from "@/domain/ArticleStatus";
import { DEFAULT_CATEGORY, FeedItem } from "@/domain/FeedItem";
import { sequence } from "@/domain/utils/sequence.promise";
import { logger } from "@/domain/utils/logger";
import { Bundle } from "@/domain/Bundle";

export class StorageCacheBuilder<T extends Object> {
  static CACHE_KEY = "__FB__CACHE__";
  private key = StorageCacheBuilder.CACHE_KEY;

  private object: T;
  private toPush: Record<string | number, any> = {};
  private dumpTimeout: number = -1;

  private static KEY = Symbol("__KEY__");
  private static TIMEOUT = Symbol("__TIMEOUT__");

  constructor(object: T, suffix: string = "") {
    this.key += suffix;

    this.object = Object.assign(
      JSON.parse(this.storage.getItem(this.key) || "{}"),
      object
    );
  }

  static get STORAGE(): Storage {
    return window.localStorage;
  }

  get storage(): Storage {
    return StorageCacheBuilder.STORAGE;
  }

  dump() {
    const item = JSON.parse(this.storage.getItem(this.key) || "{}");
    this.storage.setItem(
      this.key,
      JSON.stringify(Object.assign(item, this.toPush))
    );
    this.toPush = {};
  }

  program() {
    clearTimeout(this.dumpTimeout);
    this.dumpTimeout = window.setTimeout(this.dump.bind(this), 100);
  }

  addKeyValuePair(key: string | number, value: any) {
    this.toPush[key] = value;
    this.program();
  }

  removeKey(key: string | number) {
    if (key in this.toPush) {
      delete this.toPush[key];
      this.program();
      return;
    }

    window.setTimeout(() => {
      clearTimeout(this.dumpTimeout);
      this.dump();

      const item = JSON.parse(this.storage.getItem(this.key) || "{}");
      if (key in item) {
        delete item[key];
        this.storage.setItem(this.key, JSON.stringify(item));
      }
    }, 0);
  }

  static invalidate(...cachedObjects: Record<symbol, any>[]) {
    for (const cachedObject of cachedObjects) {
      if (
        // @ts-ignore
        !cachedObject[StorageCacheBuilder.KEY] ||
        // @ts-ignore
        !cachedObject[StorageCacheBuilder.TIMEOUT]
      ) {
        continue;
      }

      // @ts-ignore
      clearTimeout(cachedObject[StorageCacheBuilder.TIMEOUT]);
      StorageCacheBuilder.STORAGE.removeItem(
        // @ts-ignore
        cachedObject[StorageCacheBuilder.KEY]
      );
    }
  }

  build() {
    const self = this;

    return new Proxy<T>(this.object, {
      get(obj, key) {
        switch (key) {
          case StorageCacheBuilder.KEY:
            return self.key;
          case StorageCacheBuilder.TIMEOUT:
            return self.dumpTimeout;
          default:
            // @ts-ignore
            return obj[key];
        }
      },
      set(obj, key, value) {
        if (typeof key === "symbol") {
          return false;
        }

        self.addKeyValuePair.bind(self)(key, value);

        // @ts-ignore
        obj[key] = value;

        return true;
      },
      deleteProperty(obj, key) {
        if (typeof key === "symbol") {
          return false;
        }

        self.removeKey.bind(self)(key);

        // @ts-ignore
        delete obj[key];

        return true;
      },
    });
  }
}

export class CachedFeatherBrief implements IFeatherBrief {
  private static instance: CachedFeatherBrief;

  static OFFLINE_ARTICLE_ID = "PARSED_URLS__";

  private static offlineArticles = new StorageCacheBuilder<
    Record<string, FeatherBriefArticleParsed>
  >({}, CachedFeatherBrief.OFFLINE_ARTICLE_ID).build();

  public installing = false;
  public installationProgress = 1;

  private constructor(private origin: IFeatherBrief) {}

  /**
   * Clear is here to completely clean the localStorage,
   */
  public clear() {
    // We completely clean the localStorage.
    StorageCacheBuilder.invalidate(CachedFeatherBrief.offlineArticles);

    CachedFeatherBrief.offlineArticles = new StorageCacheBuilder<
      Record<string, FeatherBriefArticleParsed>
    >({}, "PARSED_URLS__").build();
  }

  public invalidate(): IFeatherBrief {
    CachedFeatherBrief.offlineArticles = new StorageCacheBuilder<
      Record<string, FeatherBriefArticleParsed>
    >(
      Object.fromEntries(
        Object.entries(this.getOfflineArticles()).filter(
          ([_, article]) => article.saved
        )
      ),
      "PARSED_URLS__"
    ).build();

    return this.origin;
  }

  static from(origin: IFeatherBrief) {
    if (!CachedFeatherBrief.instance) {
      CachedFeatherBrief.instance = new CachedFeatherBrief(origin);
    }

    return CachedFeatherBrief.instance;
  }

  getUserFeeds(): Promise<Feed[]> {
    return this.origin.getUserFeeds();
  }

  private orderByAttributes(items: FeedWithItems[]): FeedWithItems[] {
    return items.map((feed) => {
      feed.items = feed.items.sort((itemA, itemB) => {
        if (itemA.read === itemB.read) {
          return 0;
        } else if (itemA.read && !itemB.read) {
          return 1;
        } else if (itemB.read && !itemA.read) {
          return -1;
        }

        return 0;
      });

      return feed;
    });
  }

  private getOfflineArticles(): Record<string, FeatherBriefArticleParsed> {
    return CachedFeatherBrief.offlineArticles;
  }

  private getOfflineArticle(url: string): FeatherBriefArticleParsed {
    return CachedFeatherBrief.offlineArticles[url];
  }

  private isOfflineArticle(url: string) {
    return url in CachedFeatherBrief.offlineArticles;
  }

  private addOfflineArticle(url: string, article: FeatherBriefArticleParsed) {
    CachedFeatherBrief.offlineArticles[url.trim()] = article;
  }

  private deleteOfflineArticle(url: string) {
    delete CachedFeatherBrief.offlineArticles[url.trim()];
  }

  async getLastFeedItems(): Promise<FeedWithItems[]> {
    const remoteItems: FeedWithItems[] =
      (await this.origin.getLastFeedItems()) ?? [];

    const remoteArticleUrls = new Set(
      remoteItems.flatMap((f) => f.items).map((f: FeedItem) => f.articleUrl)
    );

    const feedsWithItems = Object.entries(this.getOfflineArticles())
      .filter(([url, article]) => {
        return article.feedName && !remoteArticleUrls.has(url);
      })
      .map(([url, article]) => ({
        articleUrl: url,
        title: article.title,
        saved: article.saved ?? true,
        shown: article.shown ?? false,
        read: article.read ?? false,
        feedName: article.feedName ?? "",
        feedUrl: article.feedUrl ?? "",
        category: DEFAULT_CATEGORY,
        // the next lines are filling data, it shouldn't be used
        // in the app
        author: "",
        description: "",
        downvote: false,
        upvote: false,
        email: "",
        score: 0,
        summary: "",
        date: Date.now(),
        parsable: true,
      }))
      .reduce((feedWithItems: FeedWithItems[], feedItem: FeedItem) => {
        const idx = feedWithItems.findIndex(
          (f) => f.feedUrl === feedItem.feedUrl
        );

        if (idx === -1) {
          return feedWithItems.concat({
            feedName: feedItem.feedName,
            feedUrl: feedItem.feedUrl,
            items: [feedItem],
          });
        }

        feedWithItems[idx].items.push(feedItem);
        return feedWithItems;
      }, remoteItems);

    return this.orderByAttributes(feedsWithItems);
  }

  getFeedItems(feed: string | Feed): Promise<FeedWithItems | undefined> {
    return this.origin.getFeedItems(feed);
  }

  createFeed(feed: Feed): Promise<Feed> {
    const toReturn = this.origin.createFeed(feed);
    this.invalidate();

    return toReturn;
  }

  async parseUrl(
    articleUrl: string,
    feedData: FeedWithItems[] = []
  ): Promise<FeatherBriefArticleParsed> {
    const url = articleUrl.trim();

    if (this.isOfflineArticle(url)) {
      return this.getOfflineArticle(url);
    }

    const feed: FeedItem | undefined = feedData
      .flatMap((f) => f.items)
      .find((f) => f.articleUrl === url);

    const parsedArticle = Object.assign(
      await this.origin.parseUrl(url),
      feed
        ? {
            feedName: feed.feedName,
            feedUrl: feed.feedUrl,
            read: feed.read,
            shown: feed.shown,
            saved: feed.saved,
            category: feed.category ?? DEFAULT_CATEGORY,
          }
        : {}
    );

    return parsedArticle;
  }

  private async updateItems(
    articleUrls: string | string[],
    status: ArticleStatus,
    action: "mark" | "unmark"
  ): Promise<void> {
    if (!Array.isArray(articleUrls)) {
      articleUrls = [articleUrls];
    }

    await this.origin[action](articleUrls, status);
  }

  mark(articleUrls: string | string[], status: ArticleStatus): Promise<void> {
    return this.updateItems(articleUrls, status, "mark");
  }

  unmark(articleUrls: string | string[], status: ArticleStatus): Promise<void> {
    return this.updateItems(articleUrls, status, "unmark");
  }

  async downloadArticle(
    feedItem: FeedItem,
    parsedArticle?: FeatherBriefArticleParsed
  ) {
    if (!feedItem.saved) {
      await this.mark(feedItem.articleUrl, ArticleStatus.SAVED);
    }

    const informationsToSave = {
      feedName: feedItem.feedName,
      feedUrl: feedItem.feedUrl,
      read: feedItem.read,
      shown: feedItem.shown,
      saved: true,
    };

    if (parsedArticle) {
      this.addOfflineArticle(feedItem.articleUrl, {
        ...parsedArticle,
        ...informationsToSave,
      });
      return;
    }

    const article = await this.parseUrl(feedItem.articleUrl);
    this.addOfflineArticle(feedItem.articleUrl, {
      ...article,
      ...informationsToSave,
    });
  }

  async offloadArticle(feedItem: FeedItem) {
    if (!this.isOfflineArticle(feedItem.articleUrl)) {
      return;
    }

    this.unmark(feedItem.articleUrl, ArticleStatus.SAVED);
    this.deleteOfflineArticle(feedItem.articleUrl);
  }

  async exploreFeeds() {
    return this.origin.exploreFeeds();
  }

  uploadOpml(file: File) {
    return this.origin.uploadOpml(file);
  }

  async getSavedFeedItems({ size = 100 } = {}) {
    return (await this.getLastFeedItems())
      .flatMap((f) => f.items)
      .filter((f) => f.saved)
      .slice(0, size);
  }

  installSavedFeedItems(): Promise<void> {
    const log = logger(CachedFeatherBrief).debug;

    return this.origin
      .getSavedFeedItems({ size: 100 })
      .then((feedItems) => {
        log("Installing!");
        this.installing = true;
        this.installationProgress = 0;

        return sequence(
          feedItems
            .filter((f) => !this.isOfflineArticle(f.articleUrl))
            .map((feedItem, _, { length }) => () =>
              this.downloadArticle(feedItem).then(() => {
                this.installationProgress =
                  (this.installationProgress * length + 1) / length;
                log(
                  "Installation Progress:",
                  (this.installationProgress * 100).toFixed(2) + "%"
                );
              })
            )
        );
      })
      .then(() => {
        log("Done installing!");
        this.installing = false;
        // Return void;
        return;
      });
  }

  unfollowFeed(feedUrl: string) {
    return this.origin.unfollowFeed(feedUrl);
  }

  getAllBundles(): Promise<Bundle[]> {
    return this.origin.getAllBundles();
  }

  updateFeedCategory(feed: Feed, category: string): Promise<Feed> {
    return this.origin.updateFeedCategory(feed, category);
  }

  updateFeedName(feed: Feed, name: string): Promise<Feed> {
    return this.origin.updateFeedName(feed, name);
  }
}
