import Vue from "vue";
import { Vue as VueType } from "vue/types/vue";
import { PluginObject } from "vue/types/plugin";
import { AuthService } from "@/types/Auth0VuePlugin";
import { HttpService } from "@/plugins/feather-brief/HttpService";
import { getInstance } from "@/plugins/auth/auth";
import { FeatherBrief } from "./FeatherBrief";
import { CachedFeatherBrief } from "./CachedFeatherBrief";
import { env, EnvVars as E } from "@/domain/utils/env";

interface Data {
  installing: boolean;
  installationProgress: number;
  fb: CachedFeatherBrief | null;
  interval?: number;
}

class CachedFeatherBriefFactory {
  static with($auth: AuthService, $http: HttpService) {
    // @ts-disable
    const methodNames = Object.getOwnPropertyNames(
      CachedFeatherBrief.prototype
    ).filter(
      (methodOrProperty) =>
        !/^(\$|_|constructor|installSavedFeedItems|installing|installationProgress)/.test(
          methodOrProperty
        )
    );

    const methods = Object.fromEntries(
      methodNames.map((f) => [
        f,
        function(...args: any[]) {
          // @ts-ignore
          if (this.installing) {
            return;
          }

          // @ts-ignore
          return this.fb[f].call(this.fb, ...args);
        },
      ])
    );

    return new Vue<Data, CachedFeatherBrief, {}, {}>({
      data() {
        return {
          installing: false,
          installationProgress: 1,
          fb: null,
        };
      },
      // @ts-ignore
      methods: {
        ...methods,
        async installSavedFeedItems() {
          if (this.installing) {
            return;
          }

          // this.installing = true;

          this.interval = window.setInterval(() => {
            // @ts-ignore
            this.installationProgress = this.fb.installationProgress;
            this.installing = this.installationProgress < 1;
          }, 100);

          // @ts-ignore
          return this.fb.installSavedFeedItems().finally(() => {
            this.installing = false;
            clearInterval(this.interval);
          });
        },
      },
      created() {
        this.fb = CachedFeatherBrief.from(new FeatherBrief($auth, $http));
      },
      beforeDestroy() {
        clearInterval(this.interval);
      },
    });
  }
}

export async function useFeatherBrief(): Promise<CachedFeatherBrief> {
  const $http: HttpService = new HttpService(
    await (env(E.ENV).is("production") || env(E.USE_DOMAIN).isTrue()
      ? Promise.resolve(env(E.DOMAIN).getOrElse("https://cn-ds.fr"))
      : import("@/disable-network").then((m) => m.DEFAULT_BASE_URL))
  );
  const $auth = getInstance();

  return new Promise((resolve) => {
    if (!$auth.loading) {
      return resolve(CachedFeatherBriefFactory.with($auth, $http));
    }

    $auth.$watch("loading", (loading: boolean) => {
      if (loading != undefined && !loading) {
        return resolve(CachedFeatherBriefFactory.with($auth, $http));
      }

      return resolve(
        CachedFeatherBriefFactory.with(
          ({
            getIdTokenClaims() {
              throw new Error("Not Implemented");
            },
          } as unknown) as AuthService,
          $http
        )
      );
    });
  });
}

export const FeatherBriefPlugin: PluginObject<undefined> = {
  install(vueInstance: typeof VueType) {
    return useFeatherBrief().then(($fb) => {
      vueInstance.prototype.$fb = $fb;
      $fb.installSavedFeedItems();
    });
  },
};
