import { Feed } from "@/domain/Feed";
import { FeedWithItems } from "@/domain/FeedWithItems";
import { ArticleStatus } from "@/domain/ArticleStatus";
import { FeedItem } from "@/domain/FeedItem";
import { Bundle } from "@/domain/Bundle";

export interface FeatherBriefArticleParsed {
  title: string;
  byline: string;
  content: string;
  length: number;
  excerpt: string;
  siteName: string;
  feedName?: string;
  feedUrl?: string;
  read?: boolean;
  saved?: boolean;
  shown?: boolean;
  category?: string;
}

export interface FeatherBriefArticleFailedParsing {}

export interface IFeatherBrief {
  getUserFeeds(): Promise<Feed[]>;
  getLastFeedItems(): Promise<FeedWithItems[]>;
  getFeedItems(feed: Feed | string): Promise<FeedWithItems | undefined>;
  getSavedFeedItems(opts?: { size?: number }): Promise<FeedItem[]>;
  exploreFeeds(): Promise<Feed[]>;
  createFeed(feed: Feed): Promise<Feed>;
  parseUrl(articleUrl: string): Promise<FeatherBriefArticleParsed>;
  mark(articleUrls: string | string[], status: ArticleStatus): Promise<void>;
  unmark(articleUrls: string | string[], status: ArticleStatus): Promise<void>;
  uploadOpml(file: File): Promise<void>;
  unfollowFeed(feedUrl: string): Promise<void>;
  getAllBundles(): Promise<Bundle[]>;
  updateFeedCategory(feed: Feed, category: string): Promise<Feed>;
  updateFeedName(feed: Feed, name: string): Promise<Feed>;

  downloadArticle(feedItem: FeedItem): Promise<void>;
  offloadArticle(feedItem: FeedItem): Promise<void>;
}
