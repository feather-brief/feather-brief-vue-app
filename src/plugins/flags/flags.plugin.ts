import type { PluginObject } from 'vue';
import features from '@/assets/features.json';

export const FlagsPlugin = {
  install(vm) {
    vm.prototype.$flags = features;
  }
} as PluginObject<void>
