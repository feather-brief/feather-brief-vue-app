export enum Flags {
  FEED_SETTINGS = "feed.settings",
  READER_SETTINGS = "reader.settings",
}
