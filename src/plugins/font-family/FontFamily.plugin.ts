export enum FontFamilyVars {
  Sans = "--font-family--sans",
  Serif = "--font-family--serif",
  Current = "--current-font-family",
}

export const FontFamilyPlugin = {
  install() {
    if (!document) {
      console.error(
        new Error(
          `Can't install fontFamily plugin, since document is not available`
        )
      );
      return;
    }

    const root = document.querySelector(":root") as HTMLElement;
    root.style.setProperty(
      FontFamilyVars.Serif,
      "charter, Georgia, Cambria, Baskerville, 'Times New Roman', Times, serif"
    );
    root.style.setProperty(
      FontFamilyVars.Sans,
      "Avenir, Helvetica, Arial, sans-serif"
    );
    root.style.setProperty(
      FontFamilyVars.Current,
      root.style.getPropertyValue(FontFamilyVars.Sans)
    );
  },
};
