import { EnvVars as E, env } from "@/domain/utils/env";

const shouldUseRealAuth =
  env(E.ENV).is("production") || env(E.USE_AUTH).isTrue();

export const { getInstance, useAuth0, Auth0Plugin } = require(`@/plugins/auth/${
  shouldUseRealAuth ? "auth0" : "fakeAuth"
}`);
