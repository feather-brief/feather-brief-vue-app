import Vue from "vue";
import { Vue as VueType } from "vue/types/vue";
import { PluginObject } from "vue/types/plugin";
import {
  Auth0VuePluginProperties,
  Auth0VuePluginMethods,
  Auth0VuePluginComputed,
  AuthService,
  UseAuthParameters,
} from "@/types/Auth0VuePlugin";
import { Auth0Client, IdToken } from "@auth0/auth0-spa-js";
import { logger } from "@/domain/utils/logger";

const log = logger("AuthService").debug;

let instance: AuthService;

export const getInstance: () => AuthService = () => {
  log("getInstance()");
  return instance;
};

type Main = (o?: UseAuthParameters) => AuthService;

export const useAuth0: Main = () => {
  if (instance) return instance;

  instance = new Vue<
    Auth0VuePluginProperties,
    Auth0VuePluginMethods,
    Auth0VuePluginComputed
  >({
    data: {
      loading: false,
      isAuthenticated: true,
      user: {
        email: "sarah.jones@gmail.com",
        name: "sarah.jones@gmail.com",
        picture:
          "https://images.unsplash.com/photo-1521225099409-8e1efc95321d?fit=crop&w=126&h=126&q=80&fit=facearea&facepad=3",
      },
      auth0Client: null,
      popupOpen: false,
      error: null,
    },
    computed: {
      maybeAuthClient() {
        return new Proxy(Auth0Client.prototype, {
          get() {
            return () => Promise.reject(new Error("no auth"));
          },
        }) as Auth0Client;
      },
    },
    methods: {
      async loginWithPopup() {
        return;
      },
      async handleRedirectCallback() {
        return;
      },
      async loginWithRedirect() {
        return;
      },
      async getIdTokenClaims() {
        return {
          __raw: btoa("token"),
        } as IdToken;
      },
      async getTokenSilently() {
        return "nothing";
      },
      async getTokenWithPopup() {
        return "nothing";
      },
      logout() {
        this.loading = true;

        return new Promise((r) => {
          setTimeout(() => {
            this.loading = false;
            this.isAuthenticated = false;

            log(`User is no longer authenticated`);

            r(this.$router?.replace({ name: "Home" }));
          }, 500);
        });
      },
    },
  });

  return instance;
};

export const Auth0Plugin: PluginObject<UseAuthParameters> = {
  install(vueInstance: typeof VueType, options?: UseAuthParameters) {
    log("install()");
    vueInstance.prototype.$auth = useAuth0(options);
  },
};
