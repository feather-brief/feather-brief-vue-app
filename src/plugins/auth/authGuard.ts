import { NavigationGuard } from "vue-router";
import { getInstance } from "@/plugins/auth/auth";

export const authGuard: NavigationGuard = (to, _, next) => {
  const authService = getInstance();

  const fn = () => {
    // If the user is authenticated, continue with the route
    if ("Home" === to.name && window.location.search.includes("u=")) {
      const url = new URL(window.location.href);
      const articleUrl = url.searchParams.get("u") ?? "";
      const feedName = url.searchParams.get("f") ?? "";

      return next({
        name: "SingleFeed",
        replace: true,
        params: {
          feedName,
          articleUrl,
        },
      });
    } else if (authService?.isAuthenticated) {
      return to.name === "Home" ? next({ name: "ListFeeds" }) : next();
    } else if (["Home", "SingleFeed"].includes(to.name ?? "")) {
      return next();
    }

    // Otherwise, log in
    authService
      .loginWithRedirect({ appState: { targetUrl: to.fullPath } })
      .then(() => {
        if (process.env.NODE_ENV !== "production") {
          next({ name: "Home" });
        }
      })
      .catch(console.error);
  };

  // If loading has already finished, check our auth state using `fn()`
  if (!authService?.loading) {
    return fn();
  }

  // Watch for the loading property to change before we check isAuthenticated
  authService.$watch("loading", (loading: boolean) => {
    if (loading === false) {
      return fn();
    }
  });
};
