/* eslint-disable */

import Vue from "vue";
import { PluginObject } from "vue/types/plugin";
import {
  Auth0VuePluginProperties,
  Auth0VuePluginMethods,
  Auth0VuePluginComputed,
  AuthService,
  UseAuthParameters,
} from "@/types/Auth0VuePlugin";
import createAuth0Client, {
  GetTokenSilentlyOptions,
  Auth0Client,
  RedirectLoginOptions,
  GetTokenWithPopupOptions,
  LogoutOptions,
  IdToken,
} from "@auth0/auth0-spa-js";
import { env, EnvVars as E } from "@/domain/utils/env";

/** Define a default action to perform after authentication */
const DEFAULT_REDIRECT_CALLBACK = () =>
  window.history.replaceState({}, document.title, window.location.pathname);

let instance: AuthService;

/** Returns the current instance of the SDK */
export const getInstance: () => AuthService = () => instance;

const defaultParams = {} as UseAuthParameters;

/** Creates an instance of the Auth0 SDK. If one has already been created, it returns that instance */
export const useAuth0 = ({
  onRedirectCallback = DEFAULT_REDIRECT_CALLBACK,
  redirectUri = window.location.origin + env(E.BASE_URL).getOrElse() + "feeds",
  ...options
}: UseAuthParameters = defaultParams): AuthService => {
  if (instance) return instance;

  // The 'instance' is simply a Vue object
  instance = new Vue<
    Auth0VuePluginProperties,
    Auth0VuePluginMethods,
    Auth0VuePluginComputed
  >({
    data(): Auth0VuePluginProperties {
      return {
        loading: true,
        isAuthenticated: false,
        user: {},
        auth0Client: null,
        popupOpen: false,
        error: null,
      };
    },
    computed: {
      maybeAuthClient: function() {
        return this.auth0Client
          ? this.auth0Client
          : // We return a Proxy, so we still get a valid Object that just fails more 'correctly'
            // With a promise.
            (new Proxy(Auth0Client.prototype, {
              get() {
                return () => Promise.reject(new Error("no auth"));
              },
            }) as Auth0Client);
      },
    },
    methods: {
      /** Authenticates the user using a popup window */
      async loginWithPopup(o: any) {
        this.popupOpen = true;

        try {
          await this.maybeAuthClient.loginWithPopup(o);
        } catch (e) {
          // eslint-disable-next-line
          console.error(e);
        } finally {
          this.popupOpen = false;
        }

        this.user = await this.maybeAuthClient.getUser();
        this.isAuthenticated = true;
      },
      /** Handles the callback when logging in using a redirect */
      async handleRedirectCallback() {
        this.loading = true;
        try {
          await this.maybeAuthClient.handleRedirectCallback();
          this.user = await this.maybeAuthClient.getUser();
          this.isAuthenticated = true;
        } catch (e) {
          this.error = e;
        } finally {
          this.loading = false;
        }
      },
      /** Authenticates the user using the redirect method */
      loginWithRedirect(o?: RedirectLoginOptions): Promise<void> {
        return this.maybeAuthClient.loginWithRedirect(o);
      },
      /** Returns all the claims present in the ID token */
      getIdTokenClaims(o: any): Promise<IdToken> {
        return this.maybeAuthClient.getIdTokenClaims(o);
      },
      /** Returns the access token. If the token is invalid or missing, a new one is retrieved */
      async getTokenSilently(o: GetTokenSilentlyOptions): Promise<any> {
        return this.maybeAuthClient.getTokenSilently(o);
      },
      /** Gets the access token using a popup window */
      getTokenWithPopup(o?: GetTokenWithPopupOptions): Promise<string> {
        return this.maybeAuthClient.getTokenWithPopup(o);
      },
      /** Logs the user out and removes their session on the authorization server */
      logout(o?: LogoutOptions): void | Promise<void> {
        return this.maybeAuthClient.logout(o);
      },
    },
    /** Use this lifecycle method to instantiate the SDK client */
    async created() {
      // Create a new instance of the SDK client using members of the given options object
      this.auth0Client = await createAuth0Client({
        domain: options.domain,
        // eslint-disable-next-line @typescript-eslint/camelcase
        client_id: options.clientId,
        audience: options.audience,
        // eslint-disable-next-line @typescript-eslint/camelcase
        redirect_uri: redirectUri,
      });

      try {
        // If the user is returning to the app after authentication..
        if (
          window.location.search.includes("code=") &&
          window.location.search.includes("state=")
        ) {
          // handle the redirect and retrieve tokens
          const { appState } = await this.auth0Client.handleRedirectCallback();

          // Notify subscribers that the redirect callback has happened, passing the appState
          // (useful for retrieving any pre-authentication state)
          onRedirectCallback(appState);
        }
      } catch (e) {
        this.error = e;
      } finally {
        // Initialize our internal authentication state
        this.isAuthenticated = await this.auth0Client.isAuthenticated();
        this.user = await this.auth0Client.getUser();
        this.loading = false;
      }
    },
  });

  return instance;
};

// Create a simple Vue plugin to expose the wrapper object throughout the application
export const Auth0Plugin: PluginObject<UseAuthParameters> = {
  install(vueInstance, options?: UseAuthParameters) {
    vueInstance.prototype.$auth = useAuth0(options);
  },
};
