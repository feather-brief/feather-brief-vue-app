import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import { isNil } from "lodash";
import Home from "@/views/Home.vue";
import Feeds from "@/views/Feeds.vue";
import ListFeeds from "@/views/Feeds/ListFeeds.vue";
import { authGuard } from "@/plugins/auth/authGuard";
import i18n from "@/plugins/i18n/i18n";
import { env, EnvVars as E } from "@/domain/utils/env";

const ListFeedItems = () =>
  import(/* webpackChunkName: "feeditem" */ "@/views/Feeds/ListFeedItems.vue");
const FeedItem = () =>
  import(/* webpackChunkName: "feeditem" */ "@/views/Feeds/FeedItem.vue");
const NewFeed = () => import("@/views/Feeds/NewFeed.vue");
const AddFeedsWithOpml = () => import("@/views/Feeds/AddFeedsWithOpml.vue");
const UserFeeds = () => import("@/views/Feeds/UserFeeds.vue");
const UserSettings = () =>
  import(/* webpackChunkName: "settings" */ "@/views/Feeds/UserSettings.vue");
const ReaderSettings = () =>
  import(/* webpackChunkName: "settings" */ "@/views/Feeds/ReaderSettings.vue");
const UserFeedItems = () =>
  import("@/views/Feeds/UserFeeds/ListUserFeedItems.vue");
const SingleBundle = () => import("@/views/Feeds/SingleBundle.vue");
const FeedEditCategory = () => import("@/views/Feeds/FeedEditCategory.vue");

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/feeds",
    component: Feeds,
    children: [
      {
        path: "",
        name: "ListFeeds",
        component: ListFeeds,
      },
      {
        path: ":feedName",
        name: "ListFeedItems",
        component: ListFeedItems,
        props: true,
      },
      {
        path: ":feedName/edit/category",
        name: "FeedCategory",
        component: FeedEditCategory,
        props: true,
        beforeEnter(to, _, next) {
          if (isNil(to.params.feed)) {
            return next({ name: "ListFeeds" });
          }
          return next();
        },
      },
      {
        path: "-/new",
        name: "NewFeed",
        component: NewFeed,
        meta: {
          title: i18n.t("routes.new_feed.title"),
        },
      },
      {
        path: "-/import",
        name: "AddFeedsWithOpml",
        component: AddFeedsWithOpml,
        meta: {
          title: i18n.t("routes.add_feeds.title"),
        },
      },
      {
        path: "-/list",
        name: "UserFeeds",
        component: UserFeeds,
        meta: {
          title: i18n.t("routes.user_feed.title"),
        },
      },
      {
        path: "-/list/:feedName",
        name: "UserFeedItems",
        component: UserFeedItems,
        props: true,
      },
      {
        path: "-/settings",
        name: "UserSettings",
        component: UserSettings,
        meta: {
          title: i18n.t("routes.user_settings.title"),
        },
      },
      {
        path: "-/settings/reader",
        name: "ReaderSettings",
        component: ReaderSettings,
        meta: {
          title: i18n.t("routes.reader_settings.title"),
        },
      },
      {
        path: "-/bundles/:bundle",
        name: "SingleBundle",
        component: SingleBundle,
        props: true,
      },
      {
        path: ":feedName/:articleUrl",
        name: "SingleFeed",
        component: FeedItem,
        props: true,
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: env(E.BASE_URL).getOrElse(),
  routes,
});

router.beforeEach(authGuard);

export default router;
