import Vue from "vue";
import VueI18n, { LocaleMessages } from "vue-i18n";
import fr from "./locales/fr.json";

Vue.use(VueI18n);

export default new VueI18n({
  locale: process.env.VUE_APP_I18N_LOCALE || "fr",
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || "fr",
  messages: {
    fr,
  },
});
