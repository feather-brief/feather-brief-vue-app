import Vue from "vue";
import { LogoutOptions } from "@auth0/auth0-spa-js";

interface Methods {
  logout(): Promise<void>;
}

export default Vue.extend<{}, Methods, {}, {}>({
  methods: {
    async logout() {
      // Should change the current page URL
      await this.$auth.logout({
        returnTo:
          window.location.origin + this.$router.resolve({ name: "Home" }).href,
      } as LogoutOptions);
      // The method should never get there, but just in case
      await this.$router?.replace({ name: "Home" });
    },
  },
});
