import { highlight, highlightAll, highlightElement, languages } from "prismjs";

export { highlight, highlightAll, highlightElement, languages };
