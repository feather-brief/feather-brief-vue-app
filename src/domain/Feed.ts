export interface Feed {
  feedName: string;
  feedUrl?: string;
  category?: string;
}
