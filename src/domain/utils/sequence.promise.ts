export function sequence(
  promises: ((...args: any[]) => Promise<any>)[]
): Promise<any> {
  return promises.reduce(
    (queue, job) => queue.then(job).catch(() => {}),
    Promise.resolve()
  );
}
