import { version } from "@/../package.json";
import semVerLt from "semver/functions/lt";
import { env, EnvVars } from "@/domain/utils/env";

export async function installApp() {
  const KEY = "__FB_VERSION__";

  const installedVersion = window.localStorage.getItem(KEY);
  if (!installedVersion || semVerLt(installedVersion, version)) {
    window.localStorage.clear();
    window.localStorage.setItem(KEY, version);
  }

  if (!env(EnvVars.ENV).is("production") && env(EnvVars.USE_DOMAIN).isFalse()) {
    const { worker } = require("@/disable-network.ts");
    worker().start();
  }
}
