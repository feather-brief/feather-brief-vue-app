import { generate, Pattern } from "@prescott/geo-pattern";
import { $enum } from "ts-enum-util";
import { colors } from "vuetify/lib";
import { OFFLINE_ARTICLES } from "@/domain/constants";

/**
 * @since 0.10.0
 */
export enum PatternTypes {
  Chevrons = "chevrons",
  ConcentricCircles = "concentric-circles",
  Diamonds = "diamonds",
  Hexagons = "hexagons",
  MosaicSquares = "mosaic-squares",
  NestedSquares = "nested-squares",
  Octagons = "octagons",
  OverlappingCircles = "overlapping-circles",
  OverlappingRings = "overlapping-rings",
  Plaid = "plaid",
  PlusSigns = "plus-signs",
  SineWaves = "sine-waves",
  Squares = "squares",
  Tessellation = "tessellation",
  Triangles = "triangles",
  Xes = "xes",
}

/**
 * @deprecated
 * @since 0.10.0
 */
export const GRADIENTS = [
  "linear-gradient( 179.9deg,  rgba(217,164,4,1) 10.7%, rgba(242,116,5,1) 113.2% )",
  "linear-gradient( 109.6deg,  rgba(238,58,136,1) 11.2%, rgba(128,162,245,1) 91.1% )",
  "linear-gradient( 203deg, rgba(158,252,203,1) 16.8%, rgba(63,100,255,1) 105.2% )",
];

/**
 * @deprecated
 * @since 0.10.0
 */
export const SPECIAL_GRADIENT =
  "linear-gradient( 197deg,  rgba(151,150,240,1) 12.1%, rgba(255,206,236,1) 51.2% )";

/**
 * @deprecated
 * @since 0.10.0
 * @private
 * @param word the word to hash into a number
 * @return {number} a number in [0, 1, 2] inclusive
 */
function hash(word: string): number {
  let sum = 0;
  for (let i = 0; i < word.length; i++) {
    sum += word.charCodeAt(i);
  }

  return sum % 3;
}

/**
 * @deprecated in favor of `patternFromWord`
 * @since 0.10.0
 * @see patternFromWord
 */
export function gradientFromWord(word: string) {
  if (word === OFFLINE_ARTICLES) {
    return SPECIAL_GRADIENT;
  }

  return GRADIENTS[hash(word)];
}

/**
 * @since 0.10.0
 */
export async function patternFromWord(word: string): Promise<Pattern> {
  const patterns: PatternTypes[] = $enum(PatternTypes)
    .getValues()
    .filter(
      (v) => ![PatternTypes.MosaicSquares, PatternTypes.Plaid].includes(v)
    );

  return generate({
    input: word,
    patterns,
    color: colors.grey.darken4,
  });
}
