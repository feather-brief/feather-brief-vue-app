export enum EnvVars {
  USE_DOMAIN = "VUE_APP_USE_REAL_DOMAIN",
  USE_AUTH = "VUE_APP_USE_REAL_AUTH",
  DOMAIN = "VUE_APP_DOMAIN",
  SHORT_COMMIT = "VUE_APP_CI_COMMIT_SHORT_SHA",
  JOB_DATE = "VUE_APP_CI_JOB_DATE",
  BASE_URL = "BASE_URL",
  ENV = "NODE_ENV",
  PORT = "PORT",
}

/**
 * This interface is only useful for the `get` method, and eventually
 * for the map method. Typing directly into the returned object doesn't
 * allow to type dependently of the input argument.
 * An interface allows such thing.
 *
 * FIXME: Any name more explicit?
 */
interface EnvMonad {
  /**
   * returns the value of the envvar,
   * or the `defaultValue` otherwise.
   * @param {string]} [defaultValue] the optional defaultValue to return if the envvar is not defined
   */
  getOrElse(): string | undefined;
  getOrElse(defaultValue: string): string;
  getOrElse(defaultValue: undefined): string | undefined;
  is(val: string | boolean | number | undefined): boolean;
  isTrue(): boolean;
  isFalse(): boolean;
  /**
   * Maps over the defined envvar.
   * @param {<A>(x: string) => A} mapper The mapper to call, only if the envvar is defined
   * @return {A} The mapped envvar
   *
   * ```
   * const formattedDate: string =
   *   env(EnvVars.JOB_DATE)
   *     .map(x => new Date(x).toLocaleString());
   * ```
   *
   * @see @/views/Feeds/UserSettings.vue
   */
  map<A>(mapper: (x: string) => A): A | undefined;
}

export type EnvAccessor = (x: string) => string | undefined;
const globalProcessAccessor: EnvAccessor = (x: string) => process.env[x];
export class EnvironmentVariable implements EnvMonad {
  private value: string | undefined;

  constructor(env: string, accessor: EnvAccessor = globalProcessAccessor) {
    this.value = accessor(env);
  }

  is(val: string | number | boolean | undefined) {
    return this.value?.trim() === String(val);
  }

  isTrue() {
    return this.is(true);
  }

  isFalse() {
    // This includes the case when the var is not defined,
    // Whereas this.is(false) would fail if it's not defined.
    return !this.is(true);
  }

  getOrElse(): string | undefined;
  getOrElse(defaultValue: string): string;
  getOrElse(defaultValue: undefined): string | undefined;
  getOrElse(defaultValue?: string | undefined) {
    if (this.value != undefined) {
      return this.value;
    } else {
      return defaultValue;
    }
  }

  map<A>(mapper: (x: string) => A | undefined) {
    return this.value ? mapper(this.value) : undefined;
  }
}

export function env(env: EnvVars): EnvMonad {
  return new EnvironmentVariable(env);
}
