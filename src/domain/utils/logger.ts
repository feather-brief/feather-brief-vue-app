import { env, EnvVars } from "./env";

export enum LogLevel {
  DEBUG = "debug",
  INFO = "info",
  WARNING = "warn",
  ERROR = "error",
}

export interface Logger {
  log(level: LogLevel): (message: string, ...args: any[]) => void;
  debug(message: string, ...args: any[]): void;
  info(message: string, ...args: any[]): void;
  warn(message: string, ...args: any[]): void;
  error(message: string, ...args: any[]): void;
}

export function logger(name?: any): Logger {
  const LEVEL_NAMESPACE = "@@LEVEL_NAMESPACE@@";
  let namespace: string;
  if (typeof name === "string") {
    namespace = name;
    // If name is a class
  } else if (name?.prototype?.constructor) {
    namespace = name.prototype.constructor.name;
    // if name is a function
  } else if (name?.name) {
    namespace = name.name;
  } else {
    namespace = LEVEL_NAMESPACE;
  }

  function log(level: LogLevel) {
    const prefix: string =
      namespace === LEVEL_NAMESPACE
        ? (Object.entries(LogLevel).find(([, l]) => level === l) || ["INFO"])[0]
        : namespace;

    return (message: string, ...args: any[]) => {
      if (env(EnvVars.ENV).is("development")) {
        console[level](`[${prefix}] ${message}`, ...args);
      }
    };
  }

  // @ts-ignore
  return Object.values(LogLevel).reduce(
    (functions, level: LogLevel) => {
      // @ts-ignore
      functions[level] = log(level);
      return functions;
    },
    { log }
  );
}
