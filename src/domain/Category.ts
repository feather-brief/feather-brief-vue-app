import { isEqual } from "lodash";
import { Feed } from "./Feed";
import i18n from "../plugins/i18n/i18n";

export class Category {
  public static DEFAULT_LABEL = "DEFAULT_VALUE";

  constructor(private _label: string = Category.DEFAULT_LABEL) {}

  get hasDefaultLabel(): boolean {
    return this._label === Category.DEFAULT_LABEL;
  }

  public get isDefault(): boolean {
    return this.hasDefaultLabel;
  }

  get label(): string {
    return this.hasDefaultLabel
      ? i18n.t("constants.default_category").toString()
      : this._label;
  }

  toString(): string {
    return `${this._label}`;
  }

  equals(other: Category): boolean {
    return isEqual(other, this);
  }

  clone(): Category {
    return new Category(this._label);
  }

  static fromFeed(feed: Feed): Category {
    return Category.fromApi(feed.category);
  }

  static fromApi(category: string = Category.DEFAULT_LABEL): Category {
    return new Category(category.trim());
  }

  static DEFAULT = new Category();
}
