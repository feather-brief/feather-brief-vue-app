export enum OfflineFeedItemStatus {
  ONLINE,
  OFFLINE,
  DOWNLOADING,
}
