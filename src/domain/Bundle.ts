import { Feed } from "./Feed";

export interface BundleFeed {
  name: string;
  url: string;
  publicationFrequency: string;
  lastPublicationdate: string;
  isInStanby: boolean;
  language: string;
  description: string;
  favicon: string;
}

export interface Bundle {
  name: string;
  language: string;
  feeds: BundleFeed[];
}
