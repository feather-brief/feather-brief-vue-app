import i18n from "@/plugins/i18n/i18n";

export const OFFLINE_ARTICLES = i18n.t("constants.offline_articles").toString();
