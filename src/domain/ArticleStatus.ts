export enum ArticleStatus {
  READ = "read",
  SHOWN = "shown",
  SAVED = "saved",
  UPVOTE = "upvote",
  DOWNVOTE = "downvote",
}
