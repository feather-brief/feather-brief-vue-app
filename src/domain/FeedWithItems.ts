import { filter, flow, groupBy, map, toPairs, uniqBy } from "lodash/fp";
import { Feed } from "./Feed";
import type { FeedItem } from "./FeedItem";

export interface FeedWithItems extends Feed {
  items: FeedItem[];
}

export const asFeedWithItems: (feedItems: FeedItem[]) => FeedWithItems[] = flow([
  uniqBy<FeedItem>((x: FeedItem) => x.articleUrl),
  groupBy<FeedItem>((x: FeedItem) => x.feedUrl),
  toPairs,
  filter(([feedUrl, items]) => feedUrl !== 'undefined' && items.length > 0),
  map(([feedUrl, items]: [string, FeedItem[]]) =>
    ({
      items,
      feedUrl,
      category: items[0].category,
      feedName: items[0].feedName,
    } as FeedWithItems)
  )
]);
