export const DEFAULT_CATEGORY = "DEFAULT_VALUE";

export interface FeedItem {
  articleUrl: string;
  title: string;
  description: string;
  summary: string;
  author: string;
  date: number;
  category: typeof DEFAULT_CATEGORY | string;

  feedName: string;
  feedUrl: string;

  downvote: boolean;
  email: string;
  read: boolean;
  saved: boolean;
  score: number;
  shown: boolean;
  upvote: boolean;
  parsable: boolean;
}
