import Vue from "vue";
import Vuex from "vuex";
import Component from "vue-class-component";
import App from "@/App.vue";
import vuetify from "@/plugins/vuetify/vuetify";
import router from "@/plugins/router/router";
import i18n from "@/plugins/i18n/i18n";
import { Auth0Plugin } from "@/plugins/auth/auth";
import { FeatherBriefPlugin } from "@/plugins/feather-brief/FeatherBriefPlugin";
import { FlagsPlugin } from "./plugins/flags";
import { FontFamilyPlugin } from "@/plugins/font-family/FontFamily.plugin";
import { domain, clientId } from "../auth_config.json";
import { installApp } from "@/domain/utils/installApp";
import { env, EnvVars as E } from "@/domain/utils/env";
import { registerServiceWorker } from "./registerServiceWorker";
import { useStore } from "./store/useStore";
import { Actions } from "./store/Actions";

async function main() {
  Component.registerHooks([
    "beforeRouteEnter",
    "beforeRouteUpdate",
    "beforeRouteLeave",
  ]);
  Vue.config.productionTip = false;

  Vue.use(FontFamilyPlugin);

  Vue.use(Auth0Plugin, {
    domain,
    clientId,
    onRedirectCallback(appState: { targetUrl?: string }) {
      const url = (appState?.targetUrl || window.location.pathname)
        .replace(env(E.BASE_URL).getOrElse(""), "")
        .replace(/\/$/, "");

      router.push(url.trim() || { name: "ListFeeds" });
    },
  });

  Vue.use(FeatherBriefPlugin);

  Vue.use(FlagsPlugin);

  Vue.use(Vuex);

  const store = await useStore();

  registerServiceWorker({
    onUpdate() {
      store.dispatch(Actions.didFindNewVersion);
    },
  });

  new Vue({
    vuetify,
    router,
    store,
    i18n,
    render: (h) => h(App),
  }).$mount("#app");
}

installApp().then(main);
